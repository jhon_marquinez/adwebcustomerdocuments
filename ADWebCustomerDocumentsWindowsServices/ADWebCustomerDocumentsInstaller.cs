﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;
using System.ServiceProcess;

namespace ADWebCustomerDocumentsWindowsServices
{
    [RunInstaller(true)]
    public partial class ADWebCustomerDocumentsInstaller : System.Configuration.Install.Installer
    {
        public ADWebCustomerDocumentsInstaller()
        {
            InitializeComponent();
        }

        private void ADWebCustomerDocumentsWindowsServiceInstaller_AfterInstall(object sender, InstallEventArgs e)
        {
            new ServiceController(ADWebCustomerDocumentsWindowsServiceInstaller.ServiceName).Start();
        }

        private void ADWebCustomerDocumentsWindowsServiceInstaller_BeforeInstall(object sender, InstallEventArgs e)
        {
            new ADWebWindowsServices.Manager.ADWebWindowsServicesManager().CheckConnetivityWithDatabaseEngine().CreateGeneralServicesTables();
            new ADWebCustomerDocumentsServices.Manager.ADWebServicesCustomerDocumentsManager().InsertServicesConfigurationData().InsertProcessConfigurationData().InsertTaskConfigurationData();
        }

        private void ADWebCustomerDocumentsWindowsServiceInstaller_AfterUninstall(object sender, InstallEventArgs e)
        {
            ADWebWindowsServices.Manager.ADWebWindowsServicesManager ServicesManager = new ADWebWindowsServices.Manager.ADWebWindowsServicesManager();
            ADWebCustomerDocumentsServices.Manager.ADWebServicesCustomerDocumentsManager CustomerDocumentsServiceManager = new ADWebCustomerDocumentsServices.Manager.ADWebServicesCustomerDocumentsManager();
            ServicesManager.CheckConnetivityWithDatabaseEngine();
            CustomerDocumentsServiceManager.DeleteConfigurationData();
            if (!ServicesManager.ThereIsSomeServicesInstalled())
                ServicesManager.DropGeneralServicesTables();
        }
    }
}
