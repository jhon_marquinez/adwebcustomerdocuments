﻿namespace ADWebCustomerDocumentsWindowsServices
{
    partial class ADWebCustomerDocumentsInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ADWebCustomerDocumentsWindowsServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ADWebCustomerDocumentsWindowsServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // ADWebCustomerDocumentsWindowsServiceProcessInstaller
            // 
            this.ADWebCustomerDocumentsWindowsServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ADWebCustomerDocumentsWindowsServiceProcessInstaller.Password = null;
            this.ADWebCustomerDocumentsWindowsServiceProcessInstaller.Username = null;
            // 
            // ADWebCustomerDocumentsWindowsServiceInstaller
            // 
            this.ADWebCustomerDocumentsWindowsServiceInstaller.Description = "Service to upload customer documents in Alfa Dyser S,L\' web.";
            this.ADWebCustomerDocumentsWindowsServiceInstaller.DisplayName = "Alfa Dyser customer documents services";
            this.ADWebCustomerDocumentsWindowsServiceInstaller.ServiceName = "ADWebCustomerDocumentsServices";
            this.ADWebCustomerDocumentsWindowsServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.ADWebCustomerDocumentsWindowsServiceInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.ADWebCustomerDocumentsWindowsServiceInstaller_AfterInstall);
            this.ADWebCustomerDocumentsWindowsServiceInstaller.AfterUninstall += new System.Configuration.Install.InstallEventHandler(this.ADWebCustomerDocumentsWindowsServiceInstaller_AfterUninstall);
            this.ADWebCustomerDocumentsWindowsServiceInstaller.BeforeInstall += new System.Configuration.Install.InstallEventHandler(this.ADWebCustomerDocumentsWindowsServiceInstaller_BeforeInstall);
            // 
            // ADWebCustomerDocumentsInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ADWebCustomerDocumentsWindowsServiceInstaller,
            this.ADWebCustomerDocumentsWindowsServiceProcessInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller ADWebCustomerDocumentsWindowsServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller ADWebCustomerDocumentsWindowsServiceInstaller;
    }
}