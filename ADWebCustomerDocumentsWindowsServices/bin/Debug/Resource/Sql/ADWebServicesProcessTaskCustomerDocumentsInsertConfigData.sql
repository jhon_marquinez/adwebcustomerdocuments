USE [G733_DATA]

DECLARE @IdProcess [int];
DECLARE @Exist bit;

SET @IdProcess = (SELECT [IdProcess] FROM [imp].[ADWebWindowsServicesProcess] WHERE [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsRemove');


SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcessTask] WITH (NOLOCK) 
		WHERE [IdProcess] = @IdProcess AND [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveDeliveryNote');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcessTask] ([IdProcess], [Name], [ActiveTask]) 
		VALUES (@IdProcess, N'ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveDeliveryNote', 1);
		

SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcessTask] WITH (NOLOCK) 
		WHERE [IdProcess] = @IdProcess AND [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveInvoice');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcessTask] ([IdProcess], [Name], [ActiveTask]) 
		VALUES (@IdProcess, N'ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveInvoice', 1);
		
		

SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcessTask] WITH (NOLOCK) 
		WHERE [IdProcess] = @IdProcess AND [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveOutstandingPayment');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcessTask] ([IdProcess], [Name], [ActiveTask]) 
		VALUES (@IdProcess, N'ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveOutstandingPayment', 1);





SET @IdProcess = (SELECT [IdProcess] FROM [imp].[ADWebWindowsServicesProcess] WHERE [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsCreate');


SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcessTask] WITH (NOLOCK) 
		WHERE [IdProcess] = @IdProcess AND [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskCreateDeliveryNote');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcessTask] ([IdProcess], [Name], [ActiveTask]) 
		VALUES (@IdProcess, N'ADWebWindowsServicesProcessCustomerDocumentsTaskCreateDeliveryNote', 1);
		
		
SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcessTask] WITH (NOLOCK) 
		WHERE [IdProcess] = @IdProcess AND [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskCreateInvoice');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcessTask] ([IdProcess], [Name], [ActiveTask]) 
		VALUES (@IdProcess, N'ADWebWindowsServicesProcessCustomerDocumentsTaskCreateInvoice', 1);
		
		
		
SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcessTask] WITH (NOLOCK) 
		WHERE [IdProcess] = @IdProcess AND [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskCreateOutstandingPayment');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcessTask] ([IdProcess], [Name], [ActiveTask]) 
		VALUES (@IdProcess, N'ADWebWindowsServicesProcessCustomerDocumentsTaskCreateOutstandingPayment', 1);
		
		
		
SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcessTask] WITH (NOLOCK) 
		WHERE [IdProcess] = @IdProcess AND [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskCreateSalesManOrder');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcessTask] ([IdProcess], [Name], [ActiveTask]) 
		VALUES (@IdProcess, N'ADWebWindowsServicesProcessCustomerDocumentsTaskCreateSalesManOrder', 1);
		
		
	



SET @IdProcess = (SELECT [IdProcess] FROM [imp].[ADWebWindowsServicesProcess] WHERE [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsUpload');


SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcessTask] WITH (NOLOCK) 
		WHERE [IdProcess] = @IdProcess AND [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskUploadDeliveryNote');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcessTask] ([IdProcess], [Name], [ActiveTask]) 
		VALUES (@IdProcess, N'ADWebWindowsServicesProcessCustomerDocumentsTaskUploadDeliveryNote', 1);
		
		
SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcessTask] WITH (NOLOCK) 
		WHERE [IdProcess] = @IdProcess AND [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskUploadInvoice');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcessTask] ([IdProcess], [Name], [ActiveTask]) 
		VALUES (@IdProcess, N'ADWebWindowsServicesProcessCustomerDocumentsTaskUploadInvoice', 1);
		
		
		
SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcessTask] WITH (NOLOCK) 
		WHERE [IdProcess] = @IdProcess AND [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskUploadOutstandingPayment');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcessTask] ([IdProcess], [Name], [ActiveTask]) 
		VALUES (@IdProcess, N'ADWebWindowsServicesProcessCustomerDocumentsTaskUploadOutstandingPayment', 1);
		
		
		
SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcessTask] WITH (NOLOCK) 
		WHERE [IdProcess] = @IdProcess AND [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskUploadSalesManOrder');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcessTask] ([IdProcess], [Name], [ActiveTask]) 
		VALUES (@IdProcess, N'ADWebWindowsServicesProcessCustomerDocumentsTaskUploadSalesManOrder', 1);
		

		