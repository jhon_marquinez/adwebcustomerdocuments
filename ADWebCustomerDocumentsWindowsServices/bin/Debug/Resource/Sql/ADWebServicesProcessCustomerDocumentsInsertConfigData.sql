USE [G733_DATA]

DECLARE @IdServices [int];
DECLARE @Exist bit;

SET @IdServices = (SELECT [IdServices] FROM [imp].[ADWebWindowsServices] WHERE [Name] = N'ADWebWindowsServicesCustomerDocuments');

SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcess] WITH (NOLOCK) 
		WHERE [IdServices] = @IdServices AND [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsRemove');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcess] ([IdServices],[Name],[IsProcessRunning],[ScheduleTime]) 
	VALUES (@IdServices, N'ADWebWindowsServicesProcessCustomerDocumentsRemove', 0,N'00:00,02:00,04:00,06:00,8:00,20:00,22:00');
	
	
SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcess] WITH (NOLOCK) 
		WHERE [IdServices] = @IdServices AND [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsCreate');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcess] ([IdServices],[Name],[IsProcessRunning],[ScheduleTime]) 
	VALUES (@IdServices, N'ADWebWindowsServicesProcessCustomerDocumentsCreate', 0,N'00:00,02:00,04:00,06:00,8:00,20:00,22:00');
	
	
SET @Exist = 
	(SELECT COUNT(*) FROM [imp].[ADWebWindowsServicesProcess] WITH (NOLOCK) 
		WHERE [IdServices] = @IdServices AND [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsUpload');

IF @Exist = 0 
	INSERT [imp].[ADWebWindowsServicesProcess] ([IdServices],[Name],[IsProcessRunning],[ScheduleTime]) 
	VALUES (@IdServices, N'ADWebWindowsServicesProcessCustomerDocumentsUpload', 0,N'00:00,02:00,04:00,06:00,8:00,20:00,22:00');
