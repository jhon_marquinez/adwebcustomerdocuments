USE [G733_DATA]

DELETE FROM [imp].[ADWebWindowsServicesProcessTask] WHERE [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveDeliveryNote';

DELETE FROM [imp].[ADWebWindowsServicesProcessTask] WHERE [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveInvoice';

DELETE FROM [imp].[ADWebWindowsServicesProcessTask] WHERE [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveOutstandingPayment';


DELETE FROM [imp].[ADWebWindowsServicesProcessTask] WHERE [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskCreateDeliveryNote';

DELETE FROM [imp].[ADWebWindowsServicesProcessTask] WHERE [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskCreateInvoice';

DELETE FROM [imp].[ADWebWindowsServicesProcessTask] WHERE [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskCreateOutstandingPayment';

DELETE FROM [imp].[ADWebWindowsServicesProcessTask] WHERE [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskCreateSalesManOrder';


DELETE FROM [imp].[ADWebWindowsServicesProcessTask] WHERE [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskUploadDeliveryNote';

DELETE FROM [imp].[ADWebWindowsServicesProcessTask] WHERE [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskUploadInvoice';

DELETE FROM [imp].[ADWebWindowsServicesProcessTask] WHERE [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskUploadOutstandingPayment';

DELETE FROM [imp].[ADWebWindowsServicesProcessTask] WHERE [Name] = N'ADWebWindowsServicesProcessCustomerDocumentsTaskUploadSalesManOrder';
