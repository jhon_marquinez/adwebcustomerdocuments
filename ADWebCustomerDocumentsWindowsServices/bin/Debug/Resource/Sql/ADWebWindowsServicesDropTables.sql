USE [G733_DATA]


IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesLog'))
	DROP TABLE [imp].[ADWebWindowsServicesLog]


IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessLog'))
	DROP TABLE [imp].[ADWebWindowsServicesProcessLog]


IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessTaskLog'))
	DROP TABLE [imp].[ADWebWindowsServicesProcessTaskLog]


IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessTaskConfigData'))
	DROP TABLE [imp].[ADWebWindowsServicesProcessTaskConfigData]


IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessConfigData'))
	DROP TABLE [imp].[ADWebWindowsServicesProcessConfigData]


IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessTask'))
	DROP TABLE [imp].[ADWebWindowsServicesProcessTask]


IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcessExecutionLog'))
	DROP TABLE [imp].[ADWebWindowsServicesProcessExecutionLog]


IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServicesProcess'))
	DROP TABLE [imp].[ADWebWindowsServicesProcess]


IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'imp' AND  TABLE_NAME = 'ADWebWindowsServices'))
	DROP TABLE [imp].[ADWebWindowsServices]



