﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using ADWebWindowsServices.Manager;
using ADWebWindowsServices.Entity;
using ADWebWindowsServices.Enum;
using ADTool.Log;
using ADTool.Serializer;
using ADWebCustomerDocumentsServices.Manager;

namespace ADWebCustomerDocumentsWindowsServices
{
    partial class WebCustomerDocumentsWindowsServices : ServiceBase
    {
        private System.Timers.Timer timer = new System.Timers.Timer();
        private ADWebWindowsServicesManager ServicesConfiguration = new ADWebWindowsServicesManager();

        public WebCustomerDocumentsWindowsServices()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            ADWebWindowsServicesEntity Services = null;

            try
            {
                Services = ServicesConfiguration.GetServices(new ADWebServicesCustomerDocumentsManager().GetServicesName());
                timer = new System.Timers.Timer
                {
                    Interval = Services.IntervalExecutionTimeMiliSecond
                };

                timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
                timer.Start();
            }
            catch (Exception ex)
            {
                ex = ex.InnerException ?? ex;
                ADWebWindowsServicesException ADException =
                    new ADWebWindowsServicesException(
                        ex.TargetSite.ToString(),
                        ex.Message,
                        ex.Source,
                        ex.TargetSite.DeclaringType.ToString(),
                        ex.TargetSite.MemberType.ToString(),
                        ex.StackTrace
                    );

                try { ServicesConfiguration.AddLog(Services, ADWebWindowsServicesStatus.Stopped, LogType.Error, JsonSerializer.Serialize(ADException)); }
                catch (Exception except)
                {
                    //TODO: manage exception, write error in file
                }
            }

            try { ServicesConfiguration.AddLog(Services, ADWebWindowsServicesStatus.Started); }
            catch (Exception ex)
            {
                //TODO: manage exception, write error in file
            }
        }

        protected override void OnStop()
        {
            ServicesConfiguration.AddLog(ServicesConfiguration.GetServices(new ADWebServicesCustomerDocumentsManager().GetServicesName()), ADWebWindowsServicesStatus.Stopped);
            new ADWebServicesCustomerDocumentsManager().StopAllProcessRunning();
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }


        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            ADWebServicesCustomerDocumentsManager CustomerDocumentServicesManager = null;
            ADWebWindowsServicesEntity Services = null;
            try
            {
                CustomerDocumentServicesManager = new ADWebServicesCustomerDocumentsManager();
                Services = ServicesConfiguration.GetServices(CustomerDocumentServicesManager.GetServicesName());
                timer.Interval = Services.IntervalExecutionTimeMiliSecond;
                timer.Stop();
                CustomerDocumentServicesManager.Initialize();
                timer.Start();
            }
            catch (Exception ex)
            {
                ex = ex.InnerException ?? ex;
                ADWebWindowsServicesException ADException =
                    new ADWebWindowsServicesException(
                        ex.TargetSite.ToString(),
                        ex.Message,
                        ex.Source,
                        ex.TargetSite.DeclaringType.ToString(),
                        ex.TargetSite.MemberType.ToString(),
                        ex.StackTrace
                    );
                try { ServicesConfiguration.AddLog(Services, ADWebWindowsServicesStatus.Started, LogType.Error, JsonSerializer.Serialize(ADException)); }
                catch (Exception except)
                {
                    //TODO: manage exception, write error in file
                }
                timer.Start();
            }
        }
    }
}
