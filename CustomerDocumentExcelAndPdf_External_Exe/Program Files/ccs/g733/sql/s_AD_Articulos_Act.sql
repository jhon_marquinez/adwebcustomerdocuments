select logotipo= case /*nuevo*/     when ((art.g733_estado= 3) or (DATEADD(mm, -1,  GETDATE())< opc.g733_fec_inic_art-3)) then 3
				      /*promocion */  when (art.g733_marca=2 ) then 2
				      /*oferta_sn*/	when (art.g733_ofertasn=-1) then 1 else 0 end,
bombilla = case when ( (isnull(art.g733_escalado_id,0)<>0)) then 1 else 0 end,
isnull(art.g733_ofertasn,0) ofertasn, 
art.xarticulo_id, art.g733_xdescart descripcion_art, isnull(art.g733_cat1,0) cat1, isnull(g733_cat2, 0) cat2, isnull(g733_cat3,0) as cat3, 
replace(art.xprec_venta,'.',',') precio_venta, 
replace (art.g733_preciooferta, '.',',') precio_oferta, art.g733_caja caja 
from g733_articulos art, pl_articulos_opc opc
where art.xarticulo_id=opc.xarticulo_id
and art.xempresa_id=opc.xempresa_id
and art.xempresa_id='al'
/*pacs 08/02/2016*/
and opc.g733_visible_web=-1
and opc.g733_tarifa_repre=-1
and opc.g733_estado in (0,1,3)
/*fin pacs*/
/*pacs pacs&manute 09/11/2016*/
and isnull(art.g733_marca,0) not in (3,4)
/*fin pacs&manute*/
and art.xarticulo_id in ( select sku from imp.web_rem_articulos)
and (opc.g733_invisibleweb<>-1 or (g733_invisibleweb is null or g733_invisibleweb=0))

order by 1 desc



