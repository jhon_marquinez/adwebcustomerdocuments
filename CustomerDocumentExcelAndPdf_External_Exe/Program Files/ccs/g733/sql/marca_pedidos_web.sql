select clis.xcliente_id, clis.xnombre, ac.xfecha_pedido, ac.ximporte_ped, ex.corefcli,albaranesweb.numped_id, g733_numpedweb , *
from imp.G733_dhl_exped ex left join imp.g733_dhl_palets pal
on ex.xpalet_num=pal.xpalet_num
and ex.xfecha_palet=pal.xfecha_palet
and YEAR(ex.xfecha_palet)>2017
and YEAR(pal.xfecha_palet)>2017
left join imp.g733_dhl_ficheros fich
on pal.xfichero_salida=fich.xfichero_salida
right join (
            select distinct hl.xnumdoc_id, popc.xnumdoc_id numped_id, g733_numpedweb
            from imp.pl_halbcli_lin hl right join imp.pl_hpedcli_opc popc
            on hl.xnumped_id=popc.xnumdoc_id
            and hl.xempresa_id=popc.xempresa_id
            
            and hl.xcicloped_id=popc.xciclo_id
            and hl.xtdocped_id=popc.xtipodoc_id
            and hl.xtipodoc_id=22
            and hl.xseccion_id=0
            and hl.xciclo_id>2017
            and hl.xempresa_id='al' 
            
            where  g733_origen=2
            and popc.xempresa_id='al'
            and popc.xseccion_id=0
            and popc.xtipodoc_id=21
            and popc.xciclo_id>2017
            and g733_numpedweb<>''
            
) albaranesweb
on ex.corefcli=albaranesweb.xnumdoc_id inner join
imp.pl_hpedcli_cab ac on  ac.xnumdoc_id=albaranesweb.numped_id
left join imp.pc_clientes clis
on ac.xcliente_id=clis.xcliente_id
and ac.xempresa_id=clis.xempgen_id
where clis.xempgen_id='al'
and corefcli is not null
and not exists ( select xcorefcli from imp.g733_pedidos_web  where xcorefcli=ex.corefcli and ac.xciclo_id>2017)



order by 5 desc, 6 desc, 7 desc