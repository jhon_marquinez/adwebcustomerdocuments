unit Timp_doc;

interface

uses StdCtrls, adodb, db, Classes, SysUtils, windows, dialogs, Forms, DateUtils,
  frxDBSet, frxClass, frxExportPDF, frxExportXLS, regularexpressions, uitypes;

type
  Select = (s_pedidos_cab, s_pedidos_lin, s_facturas_cab, s_facturas_lin,
            s_albaranes_cab, s_albaranes_lin);
  updates = (u_doc_imp);
  sp = (sp_facturas_cab_temp_oo);

  arr_slt = array [Select] of widestring;
  arr_upd = array [updates] of widestring;
  arr_sp = array [sp] of widestring;

  Timp_docs = class(Tobject)
  private
    { Declaraciones privadas de la clase }
    sCliIni: string;
    sClifin: string;
    SDocIni: string;
    sDocFin: string;
    sFechaIni: string;
    sFechaFin: String;
    sReimp: string; // -1:Reimpresi�n  ; 0-sin reimpresi�n  ; 1- Todos
    srepre:string; //-1: todos
    adoconnection_imp: Tadoconnection;
    frxReport_imp: TfrxReport;
    datasource_cab, datasource_lin: Tdatasource;
    datasource1, datasource2: Tdatasource;
    cons_select: arr_slt;
    // cons_upd:  arr_upd;
    cons_sp: arr_sp;
    procedure SetCliIni(valor: String);
    procedure SetCliFin(valor: String);
    procedure SetReimp(valor: String);
    procedure setRepre(valor:string);

  public
    property CliIni: String read sCliIni write SetCliIni;
    property CliFin: String read sClifin write SetCliFin;
    property DocIni: String read SDocIni write SDocIni;
    property DocFin: String read sDocFin write sDocFin;
    property FechaIni: String read sFechaIni write sFechaIni;
    property FechaFin: String read sFechaFin write sFechaFin;
    property Reimp: String read sReimp write SetReimp;
    property Conexion: Tadoconnection read adoconnection_imp
      write adoconnection_imp;
    property repre:string read srepre write setRepre;

  public
    { Declaraciones p�blicas de la clase }
    constructor create; overload;
    constructor create(cli_ini, cli_fin, doc_ini, doc_fin, fecha_ini,
      fecha_fin: string; Reimp: string); overload;
    procedure carga_consultas_array();

  private
    procedure FilenamePDF(var NomArchTemp: string);

  end;

type
  Timp_Factura = Class(Timp_docs)
  private
    sCicloIni: string;
    sCicloFin: string;
    sContabilizado: string;
    sIdent_TablaTemp: string;
    // sOrden_lin :String;

  public
    property CicloIni: String read sCicloIni write sCicloIni;
    property CicloFin: String read sCicloFin write sCicloFin;
    property Contabilizado: String read sContabilizado write sContabilizado;
    property Ident_TablaTemp: String read sIdent_TablaTemp
      write sIdent_TablaTemp;

    constructor create(cli_ini, cli_fin, doc_ini, doc_fin, fecha_ini, fecha_fin,
      ciclo_ini, ciclo_fin: string; Reimp, contab: string); overload;

    function crea_tabla_CabTemp(var adocommand_temp: Tadocommand): boolean;
    function crea_cab(var adodataset_cab: Tadodataset): boolean;
    function crea_lin(var adodataset_lin: Tadodataset;
      orden_lin: String): boolean;
    function crea_factura(var frxDBDataset_cab, frxDBDataset_lin: TfrxDBDataset;
      orden: String): boolean;

  private
    procedure Obtener_Indent(var NomArchTemp: string);

  end;

type
  Timp_pedido = class(Timp_docs)
  private
    sBis: boolean;
    sSc: boolean;


  public
    procedure setBis(valor: boolean);
    procedure setSC(valor: boolean);


    property bis: boolean read sBis write setBis;
    property sc: boolean read sSc write setSC;


    constructor create(cli_ini, cli_fin, doc_ini, doc_fin, fecha_ini, fecha_fin,
      Reimp, repre: string; bis, sc: boolean); overload;

    function crea_pedido(var frxDBDataset1, frxDBDataset2: TfrxDBDataset;
       copia_destino: string): boolean;
    //function crea_pedido(copia_destino: string): boolean;

  end;

type
  Timp_albaran = class(Timp_docs)
  private

  public
    function crea_albaran(var frxDBDataset1, frxDBDataset2: TfrxDBDataset;
      copia_destino: string): boolean;

  end;

implementation

//uses base, bdalfa, general, encripter, email;
uses base,bdalfa;


constructor Timp_docs.create;
begin
  sCliIni := '';
  sClifin := '';
  SDocIni := '';
  sDocFin := '';
  sFechaIni := '';
  sFechaFin := '';
  sReimp := '-1';
  srepre:='-1';

end;

constructor Timp_docs.create(cli_ini, cli_fin, doc_ini, doc_fin, fecha_ini,
  fecha_fin: string; Reimp: string);
begin

  SetCliIni(cli_ini);
  SetCliFin(cli_fin);
  SDocIni := SDocIni;
  sDocFin := sDocFin;
  sFechaIni := (sFechaIni);
  sFechaFin := (sFechaFin);
  SetReimp(Reimp);
  setRepre(repre);

end;

constructor Timp_Factura.create(cli_ini, cli_fin, doc_ini, doc_fin, fecha_ini,
  fecha_fin, ciclo_ini, ciclo_fin: string; Reimp, contab: string);
begin
  SetCliIni(cli_ini);
  SetCliFin(cli_fin);
  SDocIni := (SDocIni);
  sDocFin := (sDocFin);
  sFechaIni := (sFechaIni);
  sFechaFin := (sFechaFin);
  sCicloIni := (ciclo_ini);
  sCicloFin := (ciclo_fin);
  SetReimp(Reimp);
  sContabilizado := (contab);


end;

Procedure Timp_Factura.Obtener_Indent(var NomArchTemp: string);
begin

  FilenamePDF(NomArchTemp);
  sIdent_TablaTemp := '_' + NomArchTemp;

end;

procedure Timp_docs.FilenamePDF(var NomArchTemp: string);
var
  HORA, MINUTS, SEG, MSEG: word;
begin

  DecodeTime(Now, HORA, MINUTS, SEG, MSEG);
  NomArchTemp := FORMAT('%.2D%.2D%.2D%.2D', [HORA, MINUTS, SEG, MSEG]);

end;

procedure Timp_docs.carga_consultas_array();
begin
  // Cargar las consultas en array.
  cons_select[s_pedidos_cab] := retorna_consulta_ini
    ('c:\Program Files\ccs\g733\class\timp_doc.ini', 'sql_select',
    's_pedidos_cab');
  cons_select[s_pedidos_lin] := retorna_consulta_ini
    ('c:\Program Files\ccs\g733\class\timp_doc.ini', 'sql_select',
    's_pedidos_lin');
  cons_select[s_albaranes_cab] := retorna_consulta_ini
    ('c:\Program Files\ccs\g733\class\timp_doc.ini', 'sql_select',
    's_albaranes_cab');
  cons_select[s_albaranes_lin] := retorna_consulta_ini
    ('c:\Program Files\ccs\g733\class\timp_doc.ini', 'sql_select',
    's_albaranes_lin');
  cons_select[s_facturas_cab] := retorna_consulta_ini
    ('c:\Program Files\ccs\g733\class\timp_doc.ini', 'sql_select',
    's_facturas_cab');
  cons_select[s_facturas_lin] := retorna_consulta_ini
    ('c:\Program Files\ccs\g733\class\timp_doc.ini', 'sql_select',
    's_facturas_lin');
  cons_sp[sp_facturas_cab_temp_oo] :=
    retorna_consulta_ini('c:\Program Files\ccs\g733\class\timp_doc.ini', 'sp',
    'sp_facturas_cab_temp_oo');
end;

function Timp_Factura.crea_tabla_CabTemp(var adocommand_temp
  : Tadocommand): boolean;
begin

  adocommand_temp.CommandText := '';
  adocommand_temp.ParamCheck := true;
  adocommand_temp.CommandText := cons_sp[sp_facturas_cab_temp_oo];

  // adocommand_temp.CommandText:= retorna_consulta_ini('c:\Program Files\ccs\g733\class\timp_doc.ini','sp', 'sp_facturas_cab_temp_oo');
  adocommand_temp.Parameters.Refresh;

  with adocommand_temp do
  begin
    Parameters[0].Value := sIdent_TablaTemp;
    Parameters[1].Value := sFechaIni;
    Parameters[2].Value := sFechaFin;
    Parameters[3].Value := sCicloIni;
    Parameters[4].Value := sCicloFin;
    if (SDocIni = '') then
      Parameters[5].Value := ('0')
    else
      Parameters[5].Value := (SDocIni);
    if (sDocFin = '') then
      Parameters[6].Value := ('9999999')
    else
      Parameters[6].Value := (sDocFin);
    Parameters[7].Value := (sCliIni);
    Parameters[8].Value := (sClifin);
    Parameters[9].Value := (sContabilizado);
    Parameters[10].Value := (sReimp);
  end;

  try
    begin
      adocommand_temp.Execute;
      Result := true;
    end;
  except
    on E: Exception do
      Result := false;
  end;

end;

function Timp_Factura.crea_cab(var adodataset_cab: Tadodataset): boolean;
begin

  adodataset_cab.CommandText := '';
  adodataset_cab.ParamCheck := true;
  adodataset_cab.CommandText := cons_select[s_facturas_cab];
  with adodataset_cab do
  begin
    Parameters[0].Value := sIdent_TablaTemp;
    Parameters[1].Value := '0'; // Doc
    Parameters[2].Value := '0'; // ident orden de linea.
  end;

  try
    begin
      adodataset_cab.active := true;
      Result := true;
    end;
  except
    on E: Exception do
      Result := false;
  end;

end;

function Timp_Factura.crea_lin(var adodataset_lin: Tadodataset;
  orden_lin: String): boolean;
begin

  adodataset_lin.CommandText := '';
  adodataset_lin.ParamCheck := true;
  adodataset_lin.CommandText := cons_select[s_facturas_lin];
  with adodataset_lin do
  begin
    Parameters[0].Value := (sIdent_TablaTemp);
    Parameters[1].Value := ('0');
    Parameters[2].Value := (orden_lin);
  end;

  try
    begin
      adodataset_lin.active := true;
      Result := true;
    end;
  except
    on E: Exception do
      Result := false;
  end;

end;

// function Timp_docs.crea_factura(form:Tform; var adodataset_cab, adodataset_lin: Tadodataset; var frxReport_imp :TfrxReport; var frxDBDataset_cab, frxDBDataset_lin : TfrxDBDataset ; var frxPDFExport1: TfrxPDFExport ): boolean;
function Timp_Factura.crea_factura(var frxDBDataset_cab, frxDBDataset_lin
  : TfrxDBDataset; orden: String): boolean;
var
  // i: integer;
  frxPDFExport_imp: TfrxPDFExport;
  frxReport_imp : TfrxReport ;
  adodataset_cab, adodataset_lin: Tadodataset;
  adocommand_temp: Tadocommand;
  NomArchTemp: string;
begin
  Result := true;
  Obtener_Indent(NomArchTemp);

  adocommand_temp := Tadocommand.create(nil);
  adocommand_temp.Connection := adoconnection_imp;

  adodataset_cab := Tadodataset.create(nil);
  adodataset_cab.Connection := adoconnection_imp;

  adodataset_lin := Tadodataset.create(nil);
  adodataset_lin.Connection := adoconnection_imp;

  datasource_cab := Tdatasource.create(nil);
  datasource_lin := Tdatasource.create(nil);

  datasource_cab.DataSet := adodataset_cab;
  datasource_lin.DataSet := adodataset_lin;

  adodataset_lin.DataSource := datasource_cab;

  adodataset_cab.MasterFields := 'xempresa_id;xnumdoc_id;ident;';
  adodataset_lin.MasterFields := 'xempresa_id;xnumdoc_id;ident;';
  adodataset_cab.IndexFieldNames := 'xempresa_id;xnumdoc_id;ident;';
  adodataset_lin.IndexFieldNames := 'xempresa_id;xnumdoc_id;ident;';

  // for i := 0 to application.ComponentCount - 1 do
  // if application.Components[i] is TForm then
  // frxReport_imp := TfrxReport.Create(Application.Components[i]);
  frxReport_imp := TfrxReport.create(nil);

  frxDBDataset_cab.DataSource := datasource_cab;
  frxDBDataset_lin.DataSource := datasource_lin;

  frxPDFExport_imp := TfrxPDFExport.create(nil);

  carga_consultas_array();

  if crea_tabla_CabTemp(adocommand_temp) then
  begin
    if crea_cab(adodataset_cab) then
    begin
      if crea_lin(adodataset_lin, orden) then
      begin
        /// / lanzar fastreport
        If adodataset_cab.RecordCount > 0 then
        begin
          FilenamePDF(NomArchTemp);
          /// //Creamos el nombre de archivo unico
          adodataset_cab.First;
          if orden = 'emp' then
              frxReport_imp.LoadFromFile
                ('C:\Archivos de programa\ccs\g733\rpt\TFactura_Alfa_emp.fr3')
          else
               frxReport_imp.LoadFromFile
                ('C:\Archivos de programa\ccs\g733\rpt\TFactura_Alfa_cli.fr3');

          frxReport_imp.preparereport();
          frxReport_imp.Print;
          frxPDFExport_imp.ShowProgress := false;
          frxPDFExport_imp.FileName := 'I:\Fac_' + GetLoginName() + '_' +
            NomArchTemp + '.pdf';
          frxPDFExport_imp.OpenAfterExport := true;
          frxReport_imp.Export(frxPDFExport_imp);
        end;
      end;
    end;
  end;

end;

function Timp_albaran.crea_albaran(var frxDBDataset1, frxDBDataset2
  : TfrxDBDataset; copia_destino: string): boolean;
var
  i,j: integer;
  frxPDFExport_imp: TfrxPDFExport;
  adodataset1, adodataset2, adodataset_aux: Tadodataset;
  NomArchTemp: string;
  master_data,detail_data,group_data: tfrxcomponent;
  kk:tcursor;
begin
   for i := 0 to application.ComponentCount - 1 do
   if (application.Components[i]) is TForm then
     begin
        for j := 0 to application.Components[i].ComponentCount - 1 do
        begin
           if (application.Components[j]) is TButton  then
           begin
                 ////
           end;
        end;
     end;

  FilenamePDF(NomArchTemp);
  adodataset1 := Tadodataset.create(nil);
  adodataset1.Connection := adoconnection_imp;

  adodataset2 := Tadodataset.create(nil);
  adodataset2.Connection := adoconnection_imp;

  datasource1 := Tdatasource.create(nil);
  datasource2 := Tdatasource.create(nil);

  datasource1.DataSet := adodataset1;
  datasource2.DataSet := adodataset2;

  carga_consultas_array();

  adodataset1.MasterFields := 'xempresa_id;xnumdoc_id;xcliente_id;';
  adodataset2.MasterFields := 'xempresa_id;xnumdoc_id;xcliente_id;';
  adodataset1.IndexFieldNames := 'xempresa_id;xnumdoc_id;xcliente_id;';
  adodataset2.IndexFieldNames := 'xempresa_id;xnumdoc_id;xcliente_id;';

  adodataset2.DataSource := datasource1;

  frxReport_imp := TfrxReport.create(nil);
  frxDBDataset1.DataSource := datasource1;
  frxDBDataset2.DataSource := datasource2;
  frxPDFExport_imp := TfrxPDFExport.create(nil);

  adodataset_aux:= Tadodataset.Create(nil);
  adodataset_aux.Connection:=adoconnection_imp;

  if copia_destino = 'emp' then
    frxReport_imp.LoadFromFile
     ('C:\Archivos de programa\ccs\g733\rpt\Talbaranes_alfa_emp.fr3')
  else
    frxReport_imp.LoadFromFile
     ('C:\Archivos de programa\ccs\g733\rpt\Talbaranes_alfa_cli.fr3');

  with adodataset_aux do
  begin
    CommandText := '';
    ParamCheck := true;
    CommandText := cons_select[s_albaranes_cab];
    Parameters[0].Value := sFechaIni;
    Parameters[1].Value := sFechaFin;
    if (SDocIni = '') then
      Parameters[2].Value := ('0')
    else
      Parameters[2].Value := (SDocIni);
    if (sDocFin = '') then
      Parameters[3].Value := ('9999999')
    else
      Parameters[3].Value := (sDocFin);
    Parameters[4].Value := (sCliIni);
    Parameters[5].Value := (sClifin);
    Parameters[6].Value := (sReimp);
    parameters[7].Value := (repre);
  end;
  try
    adodataset_aux.active := true;
  except
    on E: Exception do
      Showmessage('Imposible Crear Cabeceras [s_albaranes_cab]' + E.Message);
  end;

  If adodataset_aux.RecordCount > 0 then
  begin
    for I := 0 to adodataset_aux.RecordCount-1 do
    begin
        with adodataset1 do
        begin
          CommandText := '';
          ParamCheck := true;
          CommandText := cons_select[s_albaranes_cab];
          Parameters[0].Value := sFechaIni;
          Parameters[1].Value := sFechaFin;
          Parameters[2].Value := adodataset_aux.fieldbyname('xnumdoc_id').asstring;
          Parameters[3].Value := adodataset_aux.fieldbyname('xnumdoc_id').asstring;
          Parameters[4].Value := (sCliIni);
          Parameters[5].Value := (sClifin);
          Parameters[6].Value := (sReimp);
          parameters[7].Value := (repre);
        end;
        try
          adodataset1.active := true;
        except
          on E: Exception do
            Showmessage('Imposible Crear Cabeceras [s_albaranes_cab]' + E.Message);
        end;

        with adodataset2 do
        begin
          DataSource := datasource1;
          CommandText := '';
          ParamCheck := true;
          CommandText := cons_select[s_albaranes_lin];
          Parameters[0].Value := sFechaIni;
          Parameters[1].Value := sFechaFin;
          Parameters[2].Value := adodataset_aux.fieldbyname('xnumdoc_id').asstring;
          Parameters[3].Value := adodataset_aux.fieldbyname('xnumdoc_id').asstring;
          Parameters[4].Value := (sCliIni);
          Parameters[5].Value := (sClifin);
          Parameters[6].Value := (sReimp);
          Parameters[7].Value := copia_destino;
          parameters[8].Value := (repre);
        end;
        try
          adodataset2.active := true;
        except
          on E: Exception do
            Showmessage('Imposible Crear Albaranes_Lin [s_albaranes_lin]' +
              E.Message);
        end;

       frxReport_imp.preparereport(false);

       adodataset1.active := false;
       adodataset2.active := false;
       adodataset_aux.Next;
     end;

    try
      begin
        frxPDFExport_imp.ShowProgress := true;
        frxPDFExport_imp.FileName := 'I:\Alb_' + NomArchTemp + '.pdf';
        frxPDFExport_imp.OpenAfterExport := true;
        frxReport_imp.Export(frxPDFExport_imp);
      end;
    except
      on E: Exception do
        Showmessage(E.ClassName +
          ' ha ocurrido un error, con el siguiente mensaje[ No se puede generar el fichero PDF -ALBARANES-] : '
          + E.Message);
    end;
  end
  else
    Showmessage('No existen ALBARANES a listar con esta selecci�n');

end;

constructor Timp_pedido.create(cli_ini, cli_fin, doc_ini, doc_fin, fecha_ini,
  fecha_fin, Reimp, repre: string; bis, sc: boolean);
begin
  SetCliIni(cli_ini);
  SetCliFin(cli_fin);
  SDocIni := (SDocIni);
  sDocFin := (sDocFin);
  sFechaIni := (sFechaIni);
  sFechaFin := (sFechaFin);
  SetReimp(Reimp);
  setBis(bis);
  setSC(sc);
end;

function Timp_pedido.crea_pedido(var frxDBDataset1, frxDBDataset2
  : TfrxDBDataset; copia_destino: string): boolean;

var
  i: integer;
  frxPDFExport_imp: TfrxPDFExport;
  adodataset1, adodataset2, adodataset_aux: Tadodataset;
  NomArchTemp: string;
  master_data,detail_data,group_data: tfrxcomponent;

begin
  Result := true;

  FilenamePDF(NomArchTemp);
  adodataset1 := Tadodataset.create(nil);
  adodataset2 := Tadodataset.create(nil);
  adodataset_aux := Tadodataset.create(nil);


  datasource1 := Tdatasource.create(nil);
  datasource2 := Tdatasource.create(nil);

  adodataset1.Connection := adoconnection_imp;
  adodataset2.Connection := adoconnection_imp;
  adodataset_aux.Connection := adoconnection_imp;

  adodataset1.MasterFields := 'xempresa_id;xnumdoc_id;xcliente_id;';
  adodataset2.MasterFields := 'xempresa_id;xnumdoc_id;xcliente_id;';
  adodataset1.IndexFieldNames := 'xempresa_id;xnumdoc_id;xcliente_id;';
  adodataset2.IndexFieldNames := 'xempresa_id;xnumdoc_id;xcliente_id;';

  datasource1.DataSet := adodataset1;
  datasource2.DataSet := adodataset2;

  adodataset2.DataSource := datasource1;

  frxReport_imp := TfrxReport.create(nil);
  frxPDFExport_imp := TfrxPDFExport.create(nil);

  frxDBDataset1.DataSource := datasource1;
  frxDBDataset2.DataSource := datasource2;

  carga_consultas_array();

  if copia_destino = 'emp' then
    frxReport_imp.LoadFromFile
      ('C:\Archivos de programa\ccs\g733\rpt\Tpedido_alfa_emp.fr3')
   else
    frxReport_imp.LoadFromFile
      ('C:\Archivos de programa\ccs\g733\rpt\Tpedido_alfa_cli.fr3');

  with adodataset_aux do
  begin
    CommandText := '';
    ParamCheck := true;
    CommandText := cons_select[s_pedidos_cab];
    Parameters[0].Value := ('AL');
    Parameters[1].Value := sFechaIni;
    Parameters[2].Value := sFechaFin;
    if (SDocIni = '') then
      Parameters[3].Value := ('0000000')
    else
      Parameters[3].Value := (SDocIni);
    if (sDocFin = '') then
      Parameters[4].Value := ('9999999')
    else
      Parameters[4].Value := (sDocFin);
    Parameters[5].Value := (sCliIni);
    Parameters[6].Value := (sClifin);
    Parameters[7].Value := BoolToStr(sBis);
    Parameters[8].Value := (sReimp);
    parameters[9].Value := (repre);
  end;
  try
    adodataset_aux.active := true;
  except
    on E: Exception do
    begin
      Result := false;
      Showmessage('Imposible Crear Cabeceras [s_pedidos_cab]' + E.Message);
    end;
  end;


  If adodataset_aux.RecordCount > 0 then
  begin
    for I := 0 to adodataset_aux.RecordCount-1 do
    begin
      with adodataset1 do
      begin
        CommandText := '';
        ParamCheck := true;
        CommandText := cons_select[s_pedidos_cab];
        Parameters[0].Value := ('AL');
        Parameters[1].Value := sFechaIni;
        Parameters[2].Value := sFechaFin;
        Parameters[3].Value := adodataset_aux.fieldbyname('xnumdoc_id').asstring;
        Parameters[4].Value := adodataset_aux.fieldbyname('xnumdoc_id').asstring;
        Parameters[5].Value := (sCliIni);
        Parameters[6].Value := (sClifin);
        Parameters[7].Value := BoolToStr(sBis);
        Parameters[8].Value := (sReimp);
        parameters[9].Value := (repre);
      end;
      try
        adodataset1.active := true;
      except
        on E: Exception do
        begin
          Result := false;
          Showmessage('Imposible Crear Cabeceras [s_pedidos_cab]' + E.Message);
        end;
      end;

      with adodataset2 do
      begin
        DataSource := datasource1;
        CommandText := '';
        ParamCheck := true;
        CommandText := cons_select[s_pedidos_lin];
        Parameters[0].Value := ('al');
        Parameters[1].Value := sFechaIni;
        Parameters[2].Value := sFechaFin;
        Parameters[3].Value := adodataset_aux.fieldbyname('xnumdoc_id').asstring;
        Parameters[4].Value := adodataset_aux.fieldbyname('xnumdoc_id').asstring;
        Parameters[5].Value := (sCliIni);
        Parameters[6].Value := (sClifin);
        Parameters[7].Value := BoolToStr(sBis);
        Parameters[8].Value := (sReimp);
        parameters[9].Value := (repre);
      end;
      try
        adodataset2.active := true;
      except
        on E: Exception do
        begin
          Result := false;
          Showmessage('Imposible Crear Pedidos_Lin [s_pedidos_lin]' + E.Message);
        end;
      end;

      frxReport_imp.preparereport(false);
      adodataset1.active := false;
      adodataset2.active := false;
      adodataset_aux.Next;
    end;

     try
      begin
        frxPDFExport_imp.ShowProgress := true;
        frxPDFExport_imp.FileName := 'I:\Ped_' + NomArchTemp + '.pdf';
        frxPDFExport_imp.OpenAfterExport := true;
        frxReport_imp.Export(frxPDFExport_imp);
      end;
     except
      on E: Exception do
        Showmessage(E.ClassName +
          ' ha ocurrido un error, con el siguiente mensaje[ No se puede generar el fichero PDF -ALBARANES-] : '
          + E.Message);
    end;
  end
  else
    Showmessage('No existen PEDIDOS a listar con esta selecci�n');

end;

procedure Timp_docs.SetCliIni(valor: String);
begin
  // a�adir al objeto los valores de los controles
  sCliIni := valor;
end;

procedure Timp_docs.SetCliFin(valor: String);
begin
  // a�adir al objeto los valores de los controles
  sClifin := valor;
end;

procedure Timp_docs.SetReimp(valor: string);
var
  RegExp_reimp: string; // '^(-1|1|0)$';
begin

  RegExp_reimp := '^(-1|1|0)$';
  try
    if TRegEx.IsMatch((valor), RegExp_reimp) then
      sReimp := valor
    else
      Showmessage('Valor de Reimpresi�n no v�lido !!!!!!!');
  except
    raise
  end;

end;

procedure Timp_pedido.setBis(valor: boolean);
var
  RegExp_bis: string; // '^(-1|1|0)$';
begin
  try
    RegExp_bis := '^(-1|0)$';
    if TRegEx.IsMatch(BoolToStr(valor), RegExp_bis) then
      sBis := valor
    else
      Showmessage('Valor Bis No V�lido');

  except
    on E: Exception do
  end;
end;

procedure Timp_pedido.setSC(valor: boolean);
var
  RegExp_bis: string; // '^(-1|1|0)$';
begin
  try
    RegExp_bis := '^(-1|0)$';
    if TRegEx.IsMatch(BoolToStr(valor), RegExp_bis) then
      sBis := valor
    else
      Showmessage('Valor Bis No V�lido');

  except
    on E: Exception do
  end;
end;


procedure Timp_docs.setRepre(valor: string);
begin
     sRepre := valor ;
 end;




end.
