﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADTool.Data;
using ADTool.Data.DapperClient;
using ADWebWindowsServices.Resource;

namespace ADWebCustomerDocumentsServices.Query
{
    internal class ADWebServicesProcessCustomerDocumentsUploadQuery : IDisposable
    {
        #region - P R O P E R T I E S
        /// <summary>
        /// property to know if has been disposed some this class's instance 
        /// </summary>
        private bool disposed = false;
        /// <summary>
        /// object to storage the object for access to database
        /// </summary>
        private DapperModel DBModel = null;
        /// <summary>
        /// object to manage the access to database
        /// </summary>
        private DapperManager DBManager = null;
        /// <summary>
        /// Connection string
        /// </summary>
        private string connectionString = String.Empty;
        /// <summary>
        /// Instance to access to data environment
        /// </summary>
        private ADWebServicesCustomerDocumentsEnvironmentQuery EnvironmentQuery = null;
        #endregion

        #region - C O N S T R U C T O R S
        /// <summary>
        /// Constructor
        /// </summary>
        public ADWebServicesProcessCustomerDocumentsUploadQuery()
        {
        }
        #endregion

        #region - M E T H O D S
        /// <summary>
        /// Initialize all instances not initialized
        /// </summary>
        private void InitializeInstances()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            if (String.IsNullOrEmpty(connectionString)) connectionString = EnvironmentQuery.GetConnectionString(ADWebWindowsServicesConstantValue.DBConnectionNameAlfaDyser, DataConnectionTypeProviderEnum.MSSQL);
            if (DBManager == null) DBManager = new DapperManager();
            if (DBModel == null) DBModel = new DapperModel();
        }

        /// <summary>
        /// Check if all instance are initialized
        /// </summary>
        /// <returns></returns>
        private bool AreInstancesLoaded()
        {
            return (EnvironmentQuery != null) && (!String.IsNullOrEmpty(connectionString)) && (DBManager != null) && (DBModel != null);
        }

        /// <summary>
        /// Get data connection FTP
        /// </summary>
        /// <returns>return data structure with required data to connect to FTP server</returns>
        public DataConnection GetFTPConnection()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetConnection("FTP");
        }

        /// <summary>
        /// Get name folder to save customer document created
        /// </summary>
        /// <returns>name folder to save customer document created</returns>
        public string GetNameFolderToSaveCustomerDocumentCreated()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetActivEnvironment().NameFolderToSaveCustomerDocumentCreated;
        }

        /// <summary>
        /// Absolute path folder root where is upload all customers documents
        /// </summary>
        /// <returns>path customer documents folder root</returns>
        public string GetPathFolderRootCustomerDocumentsOnFTPServer()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetActivEnvironment().PathFolderRootCustomerDocumentsOnFTPServer;
        }

        /// <summary>
        /// Absolute path folder root where is upload all customers documents
        /// </summary>
        /// <returns>path customer documents folder root</returns>
        public string GetPathOrderFolderOnFTPServer()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetActivEnvironment().PathOrderFolderOnFTPServer;
        }


        /// <summary>
        /// Absolute path folder root where is upload all customers documents
        /// </summary>
        /// <returns>path customer documents folder root</returns>
        public string GetPathDeliveryNoteFolderOnFTPServer()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetActivEnvironment().PathDeliveryNoteFolderOnFTPServer;
        }


        /// <summary>
        /// Absolute path folder root where is upload all customers documents
        /// </summary>
        /// <returns>path customer documents folder root</returns>
        public string GetPathInvoicesFolderOnFTPServer()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetActivEnvironment().PathInvoicesFolderOnFTPServer;
        }


        /// <summary>
        /// Absolute path folder root where is upload all customers documents
        /// </summary>
        /// <returns>path customer documents folder root</returns>
        public string GetPathCustomerOutstandingPaymentFolderOnFTPServer()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetActivEnvironment().PathCustomerOutstandingPaymentOnFTPServer;
        }


        /// <summary>
        /// Absolute path folder root where is upload all customers documents
        /// </summary>
        /// <returns>path customer documents folder root</returns>
        public string GetPathCustomerConsumptionFolderOnFTPServer()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetActivEnvironment().PathCustomerConsumption;
        }

        /// <summary>
        /// Absolute path folder root where is upload all customers order for salesman
        /// </summary>
        /// <returns>path customer documents folder root</returns>
        public string GetPathSalesmanOrders()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetActivEnvironment().PathSalesmanOrders;
        }


        #region DISPOSE METHODS
        /// <summary>
        /// ADWebServicesProcessCustomerDocumentsUploadQuery Destructor
        /// </summary>
        ~ADWebServicesProcessCustomerDocumentsUploadQuery()
        {
            Dispose(false);
        }

        /// <summary>
        /// DatabaseController Destructor
        /// </summary>
        /// <summary>
        /// Dispose the DDBB.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            /* Take yourself off the Finalization queue to prevent finalization code
             * for this object from executing a second time.
             */
            GC.SuppressFinalize(this);
        }

        /* Dispose(bool disposing) executes in two distinct scenarios.
         * If disposing equals true, the method has been called directly or indirectly
         * by a user's code. Managed and unmanaged resources can be disposed.
         * If disposing equals false, the method has been called by the runtime from
         * inside the finalizer and you should not reference other objects. Only
         * unmanaged resources can be disposed.
         */
        /// <summary>
        /// Virtual dispose the DDBB.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (DBManager != null) DBManager.Dispose();
                    if (DBModel != null) DBModel.Dispose();
                    DBManager = null;
                    DBModel = null;
                    EnvironmentQuery = null;
                    connectionString = String.Empty;
                }
                /* Release unmanaged resources. If disposing is false, only the following code
                 * is executed. Note that this is not thread safe. Another thread could start
                 * disposing the object after the managed resources are disposed, but before
                 * the disposed flag is set to true. If thread safety is necessary, it must be
                 * implemented by the client.
                 */
            }
            disposed = true;
        }
        #endregion

        #endregion

    }
}
