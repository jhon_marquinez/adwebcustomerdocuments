﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADWebCustomerDocumentsServices.Entity;
using ADWebCustomerDocumentsServices.Enum;
using ADTool.Data.DapperClient;
using ADTool.Data;
using ADWebWindowsServices.Resource;
using System.Data.SqlClient;

namespace ADWebCustomerDocumentsServices.Query
{
    internal class ADWebServicesProcessCustomerDocumentsRemoveQuery : IDisposable
    {
        #region - P R O P E R T I E S
        /// <summary>
        /// property to know if has been disposed some this class's instance 
        /// </summary>
        private bool disposed = false;
        /// <summary>
        /// object to storage the object for access to database
        /// </summary>
        private DapperModel DBModel = null;
        /// <summary>
        /// object to manage the access to database
        /// </summary>
        private DapperManager DBManager = null;
        /// <summary>
        /// Connection string
        /// </summary>
        private string connectionString = String.Empty;
        /// <summary>
        /// Instance to access to data environment
        /// </summary>
        private ADWebServicesCustomerDocumentsEnvironmentQuery EnvironmentQuery = null;
        #endregion

        #region - C O N S T R U C T O R S
        public ADWebServicesProcessCustomerDocumentsRemoveQuery()
        {
        }
        #endregion

        #region - M E T H O D S

        /// <summary>
        /// Initialize all instances not initialized
        /// </summary>
        private void InitializeInstances()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            if (String.IsNullOrEmpty(connectionString)) connectionString = EnvironmentQuery.GetConnectionString(ADWebWindowsServicesConstantValue.DBConnectionNameAlfaDyser, DataConnectionTypeProviderEnum.MSSQL);
            if (DBManager == null) DBManager = new DapperManager();
            if (DBModel == null) DBModel = new DapperModel();
        }

        /// <summary>
        /// Check if all instance are initialized
        /// </summary>
        /// <returns></returns>
        private bool AreInstancesLoaded()
        {
            return (EnvironmentQuery != null) && (!String.IsNullOrEmpty(connectionString)) && (DBManager != null) && (DBModel != null);
        }

        /// <summary>
        /// Get all invoices document to remove
        /// </summary>
        /// <returns>List of ADWebServicesProcessCustomerDocumentEntity object with the data about invoices documents</returns>
        public IEnumerable<ADWebServicesProcessCustomerDocumentEntity> GetInvoiceToRemove()
        {
            IEnumerable<ADWebServicesProcessCustomerDocumentEntity> result = null;
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"set dateformat dmy  
                            select xcliente_id Customer, xnumdoc_id IdDocument, convert(char(10),xfecha_doc, 103) as Date, xborrado IsRemoved, xtipodoc_id TypeDocument, ximporte Import, xformato TypeFile 
                            from imp.web_docs_ftp where cast(xfecha_doc as date)<cast(getdate()-180 as DATE) and xborrado=0 and xtipodoc_id={(int)TypeDocument.Invoice} 
                            order by Date";
            DBModel.Connection = new SqlConnection { ConnectionString = DBModel.ConnectionString = connectionString };
            result = DBManager.Query<ADWebServicesProcessCustomerDocumentEntity>(DBModel, query);
            return result;
        }

        /// <summary>
        /// Get all delivery note document to remove
        /// </summary>
        /// <returns>List of ADWebServicesProcessCustomerDocumentEntity object with the data about delivery note documents</returns>
        public IEnumerable<ADWebServicesProcessCustomerDocumentEntity> GetDeliveryNoteToRemove()
        {
            IEnumerable<ADWebServicesProcessCustomerDocumentEntity> result = null;
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"select xcliente_id Customer, xnumdoc_id IdDocument, convert(char(10),xfecha_doc, 103) as Date, xborrado IsRemoved, xtipodoc_id TypeDocument, ximporte Import, xformato TypeFile 
                            from imp.web_docs_ftp 
                            where xnumdoc_id not in ( select xnumdoc_id from imp.pl_albcli_cab) and xborrado=0 and xtipodoc_id=22 and (((xcliente_id=41743) and  (cast(xfecha_doc as date)>cast(getdate()-10 as DATE))) or (xcliente_id<>41743)) 
                            order by 2";
            DBModel.Connection = new SqlConnection { ConnectionString = DBModel.ConnectionString = connectionString };
            result = DBManager.Query<ADWebServicesProcessCustomerDocumentEntity>(DBModel, query);
            return result;
        }

        /// <summary>
        /// Get all customer effect document to set like removed
        /// </summary>
        /// <returns>Collection of ADWebServicesProcessCustomerDocumentEffectEtntity objects with data about effects documents</returns>
        public IEnumerable<ADWebServicesProcessCustomerDocumentEffectEtntity> GetEffectsToSetLikeDeleted()
        {
            IEnumerable<ADWebServicesProcessCustomerDocumentEffectEtntity> result = null;
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"execute imp.Web_s_rel_efectos_actualizar 1";
            DBModel.Connection = new SqlConnection { ConnectionString = DBModel.ConnectionString = connectionString };
            result = DBManager.Query<ADWebServicesProcessCustomerDocumentEffectEtntity>(DBModel, query);
            return result;
        }

        /// <summary>
        /// Get collection string with customer code for removing outstanding payment file on FTP server
        /// </summary>
        /// <returns>Collection string with code customer</returns>
        public IEnumerable<string> GetCustomerToRemoveOutstandingPaymentsFile()
        {
            IEnumerable<string> result = null;
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"select xcliente_id from imp.web_docs_ftp_efectos_ptes where xborrado=-1 and xfecha_modificado >= CONVERT (date, GETDATE()) except select xcliente_id from imp.web_docs_ftp_efectos_ptes where xborrado=0 order by 1";
            DBModel.Connection = new SqlConnection { ConnectionString = DBModel.ConnectionString = connectionString };
            result = DBManager.Query<string>(DBModel, query);
            return result;
        }

        /// <summary>
        /// Set document effect like removed
        /// </summary>
        public void SetEffectsLikeDeleted(ADWebServicesProcessCustomerDocumentEffectEtntity DocumentEffectEtntity)
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            string query = $@"update imp.web_docs_ftp_efectos_ptes set xborrado='-1', xfecha_modificado=GETDATE() 
                            where xcliente_id=@cuenta and xempresa_id='AL'and xdocumento_id=@xdocumento_id and xfecha_vencimiento=@fecha_vto";
            DBModel.Connection = new SqlConnection { ConnectionString = DBModel.ConnectionString = connectionString };
            DBManager.Update(DBModel, query, DocumentEffectEtntity);
        }

        /// <summary>
        /// Save the log indicating invoice has been removed
        /// </summary>
        /// <param name="CustomerDocumentEntity">Document information</param>
        public void AddLogDocumentRemoved(ADWebServicesProcessCustomerDocumentEntity CustomerDocumentEntity)
        {
            if (!AreInstancesLoaded()) InitializeInstances();
            int invoiceRemoved = CustomerDocumentEntity.IsRemoved ? -1 : 0;
            string query = $@"update imp.web_docs_ftp 
                            set xfecha_doc = @Date, xborrado = {invoiceRemoved}, ximporte = @Import, xfecha_modificado = getdate() 
                            where xcliente_id = @Customer and xnumdoc_id = @IdDocument and xtipodoc_id = @TypeDocument and xformato = @TypeFile";
            DBModel.Connection = new SqlConnection { ConnectionString = DBModel.ConnectionString = connectionString };
            DBManager.Update(DBModel, query, CustomerDocumentEntity);
        }

        /// <summary>
        /// Get data connection FTP
        /// </summary>
        /// <returns>return data structure with required data to connect to FTP server</returns>
        public DataConnection GetFTPConnection()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetConnection("FTP");
        }

        /// <summary>
        /// Get absolute path of invoices folder on FTP server
        /// </summary>
        /// <returns>Path of invoices folder on FTP server</returns>
        public string GetPathFolderInvoicesOnFTPServer()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetActivEnvironment().PathInvoicesFolderOnFTPServer;
        }

        /// <summary>
        /// Get absolute path of delivery notes folder on FTP server
        /// </summary>
        /// <returns>Path of delivery notes folder on FTP server</returns>
        public string GetPathFolderDeliveryNoteOnFTPServer()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetActivEnvironment().PathDeliveryNoteFolderOnFTPServer;
        }

        /// <summary>
        /// Get structure name of invoices
        /// </summary>
        /// <returns>Structure name of invoices</returns>
        public string GetStructureNameOfInvoice()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetActivEnvironment().NameFileInvoice;
        }

        /// <summary>
        /// Get structure name of delivery notes
        /// </summary>
        /// <returns>Structure name of delivery notes</returns>
        public string GetStructureNameOfDeliveryNote()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetActivEnvironment().NameFileDeliveryNote;
        }

        /// <summary>
        /// Absolute path where is the customer outstanding payment
        /// </summary>
        /// <returns></returns>
        public string GetAbsolutePathCustomerOutstandingPaymentOnFTPServer()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetActivEnvironment().PathCustomerOutstandingPaymentOnFTPServer;
        }

        /// <summary>
        /// /// Absolute path where is the software to create HTML table of customer document
        /// </summary>
        /// <returns>C:\Program Files\ccs\g733\ejecutables\CustomerDocumentExcelAndPdf.exe /ps:{0} /ci:{1} /td:{2} /ch:1</returns>
        public string GetAbsolutePathOfSoftwareToCreateCustomerTableHTMLDocument()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetActivEnvironment().FunctionalModuleToCreateCustomerDocuments;
        }

        /// <summary>
        /// Get name folder to save customer document created
        /// </summary>
        /// <returns></returns>
        public string GetNameFolderToSaveCustomerDocumentCreated()
        {
            if (EnvironmentQuery == null) EnvironmentQuery = new ADWebServicesCustomerDocumentsEnvironmentQuery();
            return EnvironmentQuery.GetActivEnvironment().NameFolderToSaveCustomerDocumentCreated;
        }

        #region DISPOSE METHODS
        /// <summary>
        /// ADWebServicesProcessCustomerDocumentsRemoveQuery Destructor
        /// </summary>
        ~ADWebServicesProcessCustomerDocumentsRemoveQuery()
        {
            Dispose(false);
        }

        /// <summary>
        /// DatabaseController Destructor
        /// </summary>
        /// <summary>
        /// Dispose the DDBB.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            /* Take yourself off the Finalization queue to prevent finalization code
             * for this object from executing a second time.
             */
            GC.SuppressFinalize(this);
        }

        /* Dispose(bool disposing) executes in two distinct scenarios.
         * If disposing equals true, the method has been called directly or indirectly
         * by a user's code. Managed and unmanaged resources can be disposed.
         * If disposing equals false, the method has been called by the runtime from
         * inside the finalizer and you should not reference other objects. Only
         * unmanaged resources can be disposed.
         */
        /// <summary>
        /// Virtual dispose the DDBB.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (DBManager != null) DBManager.Dispose();
                    if (DBModel != null) DBModel.Dispose();
                    DBManager = null;
                    DBModel = null;
                    EnvironmentQuery = null;
                    connectionString = String.Empty;
                }
                /* Release unmanaged resources. If disposing is false, only the following code
                 * is executed. Note that this is not thread safe. Another thread could start
                 * disposing the object after the managed resources are disposed, but before
                 * the disposed flag is set to true. If thread safety is necessary, it must be
                 * implemented by the client.
                 */
            }
            disposed = true;
        }
        #endregion

        #endregion
    }
}
