﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADTool.Environment;
using ADTool.CustomFile;
using ADTool.Data;
using System.IO;
using ADWebCustomerDocumentsServices.Entity;

namespace ADWebCustomerDocumentsServices.Query
{
    internal class ADWebServicesCustomerDocumentsEnvironmentQuery
    {
        #region - P R O P E R T I E S
        /// <summary>
        /// Private environment manager.
        /// </summary>
        private EnvironmentManager<ADWebServicesEnvironmentEntity> envManager = null;
        /// <summary>
        /// Environment file name.
        /// </summary>
        private const string envConfigFileName = "ConfigFile\\ADWebWindowsServicesCustomerDocumentsEnv.cfg";
        #endregion

        #region - C O N S T R U C T O R S 
        /// <summary>
        /// Constructor.
        /// </summary>
        public ADWebServicesCustomerDocumentsEnvironmentQuery()
        {
            envManager = new EnvironmentManager<ADWebServicesEnvironmentEntity>();
        }
        #endregion

        #region - M E T H O D S
        /// <summary>
        /// Load the environment if it's not loaded yet.
        /// </summary>
        /// <exception cref="Exception"></exception>
        private void LoadEnvironmentData()
        {
            if (!envManager.IsEnvironmentLoaded())
            {
                string pathEnvConfig = Path.Combine(FileManager.GetApplicationPath(), envConfigFileName);
                envManager.LoadEnvironment(pathEnvConfig);
            }
        }
        /// <summary>
        /// Get the connection string.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public string GetConnectionString(string connectionName, DataConnectionTypeProviderEnum typeProviderEnum)
        {
            LoadEnvironmentData();
            return envManager.ActivEnvironment().Connections.Where(conn => conn.ConnectionName == connectionName).First().GetConnectionString(typeProviderEnum);
        }
        /// <summary>
        /// Get the connection object.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public DataConnection GetConnection(string connectionName)
        {
            LoadEnvironmentData();
            return envManager.ActivEnvironment().Connections.Where(conn => conn.ConnectionName == connectionName).First();
        }
        /// <summary>
        /// Get the active environment.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public ADWebServicesEnvironmentEntity GetActivEnvironment()
        {
            LoadEnvironmentData();
            return envManager.ActivEnvironment();
        }
        #endregion
    }
}
