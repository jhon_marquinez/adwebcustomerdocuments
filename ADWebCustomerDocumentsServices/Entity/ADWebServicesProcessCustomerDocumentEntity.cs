﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADWebCustomerDocumentsServices.Enum;

namespace ADWebCustomerDocumentsServices.Entity
{
    internal class ADWebServicesProcessCustomerDocumentEntity
    {
        #region - P R O P E R T I E S
        /// <summary>
        /// Code customer
        /// </summary>
        public string Customer { get; set; }

        /// <summary>
        /// Id document
        /// </summary>
        public int IdDocument { get; set; }

        /// <summary>
        /// Date document done
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Field to know if this document has been removed
        /// </summary>
        public bool IsRemoved { get; set; }

        /// <summary>
        /// Type document
        /// </summary>
        public TypeDocument TypeDocument { get; set; }

        /// <summary>
        /// Import
        /// </summary>
        public double Import { get; set; }
        
        /// <summary>
        /// File type
        /// </summary>
        public string TypeFile { get; set; }
        #endregion

        #region - C O N S T R U C T O R S
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="customer">Filed to know customer id</param>
        /// <param name="idDocument">Field to know document id</param>
        /// <param name="date">Field to know date document</param>
        /// <param name="isRemoved">Field to know if document has been removed</param>
        /// <param name="typeDocument">Field to know type document</param>
        /// <param name="import">Filed to know import</param>
        /// <param name="typeFile">Filed to know type file</param>
        public ADWebServicesProcessCustomerDocumentEntity(string customer, int idDocument, DateTime date, bool isRemoved, TypeDocument typeDocument, double import, string typeFile)
        {
            Customer = customer;
            IdDocument = idDocument;
            Date = date;
            IsRemoved = isRemoved;
            TypeDocument = typeDocument;
            Import = import;
            TypeFile = typeFile;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public ADWebServicesProcessCustomerDocumentEntity()
        {
        }
        #endregion
    }
}
