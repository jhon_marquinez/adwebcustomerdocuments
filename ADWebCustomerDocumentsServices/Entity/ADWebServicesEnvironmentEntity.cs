﻿using ADTool.Environment;
using ADTool.Data;
using System.Collections.Generic;

namespace ADWebCustomerDocumentsServices.Entity
{
    internal class ADWebServicesEnvironmentEntity : IEnvironment
    {
        #region - P R O P E R T I E S
        /// <summary>
        /// Environment name
        /// </summary>
        public string EnvironmentName { get; set; }
        /// <summary>
        /// Active environment
        /// </summary>
        public bool ActiveEnvironment { get; set; }
        /// <summary>
        /// Type environment
        /// </summary>
        public DataEnvironmentTypeEnum DataEnvironmentType { get; set; }
        /// <summary>
        /// Connections
        /// </summary>
        public IEnumerable<DataConnection> Connections { get; set; }
        /// <summary>
        /// Services name
        /// </summary>
        public string ServicesName { get; set; }
        /// <summary>
        /// SQL to insert configuration data of services
        /// </summary>
        public string ADWebServicesCustomerDocumentsInsertConfigData { get; set; }
        /// <summary>
        /// SQL to delete configuration data of services
        /// </summary>
        public string ADWebServicesCustomerDocumentsDeleteConfigData { get; set; }
        /// <summary>
        /// SQL to insert configuration data of process
        /// </summary>
        public string ADWebServicesProcessCustomerDocumentsInsertConfigData { get; set; }
        /// <summary>
        /// SQL to insert configuration data of tasks
        /// </summary>
        public string ADWebServicesProcessTaskCustomerDocumentsInsertConfigData { get; set; }
        /// <summary>
        /// Path folder invoices on FTP server
        /// </summary>
        public string PathInvoicesFolderOnFTPServer { get; set; }
        /// <summary>
        /// Path folder delivery notes on FTP server
        /// </summary>
        public string PathDeliveryNoteFolderOnFTPServer { get; set; }
        /// <summary>
        /// structure name of invoices
        /// </summary>
        public string NameFileInvoice { get; set; }
        /// <summary>
        /// /// structure name of delivery notes
        /// </summary>
        public string NameFileDeliveryNote { get; set; }
        /// <summary>
        /// Absolute path where is the customer outstanding payment
        /// </summary>
        public string PathCustomerOutstandingPaymentOnFTPServer { get; set; }
        /// <summary>
        /// Absolute path where is the software to create HTML table of customer document
        /// </summary>
        public string FunctionalModuleToCreateCustomerDocuments { get; set; }
        /// <summary>
        /// Name folder to save customer document created
        /// </summary>
        public string NameFolderToSaveCustomerDocumentCreated { get; set; }
        /// <summary>
        /// Absolute path where is the documents folder customers
        /// </summary>
        public string AbsolutePathToSaveCustomerDocumentCreated { get; set; }
        /// <summary>
        /// Absolute path folder root where is upload all customers documents
        /// </summary>
        public string PathFolderRootCustomerDocumentsOnFTPServer { get; set; }
        /// <summary>
        /// Path customer order folder on FTP Server
        /// </summary>
        public string PathOrderFolderOnFTPServer { get; set; }
        /// <summary>
        /// Path customer consumption folder on FTP Server
        /// </summary>
        public string PathCustomerConsumption { get; set; }
        /// <summary>
        /// Path salesman orders
        /// </summary>
        public string PathSalesmanOrders { get; set; }
        #endregion
    }
}
