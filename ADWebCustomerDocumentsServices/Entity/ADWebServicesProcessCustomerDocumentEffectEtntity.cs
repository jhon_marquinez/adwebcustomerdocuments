﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADWebCustomerDocumentsServices.Entity
{
    internal class ADWebServicesProcessCustomerDocumentEffectEtntity
    {
        #region - P R O P E R T I E S
        /// <summary>
        /// Code customer
        /// </summary>
        public string cuenta { get; set; }

        /// <summary>
        /// Id document
        /// </summary>
        public string xdocumento_id { get; set; }

        /// <summary>
        /// Date document done
        /// </summary>
        public DateTime fecha_vto { get; set; }
        #endregion

        #region - C O N S T R U C T O R S
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="cuenta">Field to know customer id</param>
        /// <param name="xdocumento_id">Field to know document id</param>
        /// <param name="fecha_vto">Field to know expiration date outstanding payment</param>
        public ADWebServicesProcessCustomerDocumentEffectEtntity(string cuenta, string xdocumento_id, DateTime fecha_vto)
        {
            this.cuenta = cuenta;
            this.xdocumento_id = xdocumento_id;
            this.fecha_vto = fecha_vto;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public ADWebServicesProcessCustomerDocumentEffectEtntity()
        {
        }
        #endregion




    }
}
