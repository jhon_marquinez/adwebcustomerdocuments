﻿
namespace ADWebCustomerDocumentsServices.Resouce
{
    internal class ConstantValue
    {
        public static string ADWebWindowsServicesProcessCustomerDocumentsRemove = "ADWebWindowsServicesProcessCustomerDocumentsRemove";
        public static string ADWebWindowsServicesProcessCustomerDocumentsCreate = "ADWebWindowsServicesProcessCustomerDocumentsCreate";
        public static string ADWebWindowsServicesProcessCustomerDocumentsUpload = "ADWebWindowsServicesProcessCustomerDocumentsUpload";

        public static string ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveDeliveryNote = "ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveDeliveryNote";
        public static string ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveInvoice = "ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveInvoice";
        public static string ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveOutstandingPayment = "ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveOutstandingPayment";

        public static string ADWebWindowsServicesProcessCustomerDocumentsTaskCreateDeliveryNote = "ADWebWindowsServicesProcessCustomerDocumentsTaskCreateDeliveryNote";
        public static string ADWebWindowsServicesProcessCustomerDocumentsTaskCreateInvoice = "ADWebWindowsServicesProcessCustomerDocumentsTaskCreateInvoice";
        public static string ADWebWindowsServicesProcessCustomerDocumentsTaskCreateOutstandingPayment = "ADWebWindowsServicesProcessCustomerDocumentsTaskCreateOutstandingPayment";
        public static string ADWebWindowsServicesProcessCustomerDocumentsTaskCreateSalesManOrder = "ADWebWindowsServicesProcessCustomerDocumentsTaskCreateSalesManOrder";

        public static string ADWebWindowsServicesProcessCustomerDocumentsTaskUploadDeliveryNote = "ADWebWindowsServicesProcessCustomerDocumentsTaskUploadDeliveryNote";
        public static string ADWebWindowsServicesProcessCustomerDocumentsTaskUploadInvoice = "ADWebWindowsServicesProcessCustomerDocumentsTaskUploadInvoice";
        public static string ADWebWindowsServicesProcessCustomerDocumentsTaskUploadOutstandingPayment = "ADWebWindowsServicesProcessCustomerDocumentsTaskUploadOutstandingPayment";
        public static string ADWebWindowsServicesProcessCustomerDocumentsTaskUploadSalesManOrder = "ADWebWindowsServicesProcessCustomerDocumentsTaskUploadSalesManOrder";

        public static string PDFExtention = "pdf";
        public static string ExcelExtention = "xls";
        public static string PDFName = "PDF";
        public static string ExcelName = "EXCEL";

        public static string DeliveryNoteNameFolderLocal = "\\2_Albaranes\\";
        public static string InvoicesNameFolderLocal = "\\3_Facturas\\";
        public static string OutstandingPaymentsNameFolderLocal = "\\4_Pagos Pendientes\\";
        public static string CustomerConsumptionNameFolderLocal = "\\5_Articulos Comprados\\";
        public static string SalesManOrdersNameFolderLocal = "\\Pedidos Clientes\\";

        public static string NameFileHtmlInvoicesTable = "Facturas.html";
        public static string NameFileHtmlDeliveryNoteTable = "Albaranes.html";

        public static string PametersToCreateTableHtml = "/ps:{0} /ci:{1} /td:{2} /ch:1";
        public static string PametersToCreateDeliveryNoteDocuments = "/ps:{0} /td:{1} /re:-1";
        public static string PametersToCreateInvoiceDocuments = "/ps:{0} /td:{1} /ic:{2} /fc:{3} /ct:-1 /re:1";
        public static string PametersToCreateOutstandingPaymentDocuments = "/ps:{0} /td:{1}";
        public static string PametersToCreateSalesmanOrdersDocuments = "/ps:{0} /td:{1}";

        public static string PrefixCodeSalesMan = "660";
    }
}
