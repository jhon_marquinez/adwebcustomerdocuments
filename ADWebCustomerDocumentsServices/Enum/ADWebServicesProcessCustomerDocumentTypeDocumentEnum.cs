﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADWebCustomerDocumentsServices.Enum
{
    internal enum TypeDocument
    {
        /// <summary>
        /// Value to identify order document
        /// </summary>
        Order = 21,
        /// <summary>
        /// Value to identify delivery note document
        /// </summary>
        DeliveryNote = 22,
        /// <summary>
        /// Value to identify invoice document
        /// </summary>
        Invoice = 23,
        /// <summary>
        /// Value to identify salesman order document
        /// </summary>
        SalesmanOrder = 5,
        /// <summary>
        /// Value to identify outstanding payment document
        /// </summary>
        OutstandingPayment = 6

    }
}
