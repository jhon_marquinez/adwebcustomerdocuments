﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADWebCustomerDocumentsServices.Query;

namespace ADWebCustomerDocumentsServices.Repository
{
    internal class ADWebServicesProcessCustomerDocumentsCreateRepository : IDisposable
    {
        #region - P R O P E R T I E S
        /// <summary>
        /// property to know if has been disposed some this class's instance 
        /// </summary>
        private bool disposed = false;
        /// <summary>
        /// Instance to access to data base
        /// </summary>
        private ADWebServicesProcessCustomerDocumentsCreateQuery CustomerDocumentsCreateQuery = null;
        #endregion

        #region - C O N S T R U C T O R
        /// <summary>
        /// Constructor
        /// </summary>
        public ADWebServicesProcessCustomerDocumentsCreateRepository()
        {
            CustomerDocumentsCreateQuery = new ADWebServicesProcessCustomerDocumentsCreateQuery();
        }
        #endregion

        #region - M E T H O D S 

        /// <summary>
        /// Get name folder to save customer document created
        /// </summary>
        /// <returns></returns>
        public string GetNameFolderToSaveCustomerDocumentCreated()
        {
            return CustomerDocumentsCreateQuery.GetNameFolderToSaveCustomerDocumentCreated();
        }

        /// <summary>
        /// Absolute path where is the software to create HTML table of customer document
        /// </summary>
        /// <returns>C:\Program Files\ccs\g733\ejecutables\CustomerDocumentExcelAndPdf.exe</returns>
        public string GetAbsolutePathOfSoftwareToCreateCustomerDocuments()
        {
            return CustomerDocumentsCreateQuery.GetAbsolutePathOfSoftwareToCreateCustomerDocuments();
        }

        /// <summary>
        /// Get Absolute path where is the documents folder customers
        /// </summary>
        /// <returns></returns>
        public string GetAbsolutePathToSaveCustomerDocumentCreated()
        {
            return new ADWebServicesCustomerDocumentsQuery().GetAbsolutePathToSaveCustomerDocumentCreated();
        }

        #region DISPOSE METHODS
        /// <summary>
        /// ADWebServicesProcessCustomerDocumentsCreateRepository Destructor
        /// </summary>
        ~ADWebServicesProcessCustomerDocumentsCreateRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// DatabaseController Destructor
        /// </summary>
        /// <summary>
        /// Dispose the DDBB.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            /* Take yourself off the Finalization queue to prevent finalization code
             * for this object from executing a second time.
             */
            GC.SuppressFinalize(this);
        }

        /* Dispose(bool disposing) executes in two distinct scenarios.
         * If disposing equals true, the method has been called directly or indirectly
         * by a user's code. Managed and unmanaged resources can be disposed.
         * If disposing equals false, the method has been called by the runtime from
         * inside the finalizer and you should not reference other objects. Only
         * unmanaged resources can be disposed.
         */
        /// <summary>
        /// Virtual dispose the DDBB.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    CustomerDocumentsCreateQuery.Dispose();
                    CustomerDocumentsCreateQuery = null;
                }
                /* Release unmanaged resources. If disposing is false, only the following code
                 * is executed. Note that this is not thread safe. Another thread could start
                 * disposing the object after the managed resources are disposed, but before
                 * the disposed flag is set to true. If thread safety is necessary, it must be
                 * implemented by the client.
                 */
            }
            disposed = true;
        }
        #endregion

        #endregion
    }
}
