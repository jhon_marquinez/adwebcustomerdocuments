﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ADWebCustomerDocumentsServices.Query;
using ADWebCustomerDocumentsServices.Entity;
using ADWebCustomerDocumentsServices.Resouce;

namespace ADWebCustomerDocumentsServices.Repository
{
    internal class ADWebServicesProcessCustomerDocumentsRemoveRepository : IDisposable
    {
        #region - P R O P E R T I E S
        /// <summary>
        /// property to know if has been disposed some this class's instance 
        /// </summary>
        private bool disposed = false;
        /// <summary>
        /// Instance to access to data base
        /// </summary>
        private ADWebServicesProcessCustomerDocumentsRemoveQuery CustomerDocumentsRemoveQuery = null;
        #endregion

        #region - C O N S T R U C T O R
        /// <summary>
        /// Constructor
        /// </summary>
        public ADWebServicesProcessCustomerDocumentsRemoveRepository()
        {
            CustomerDocumentsRemoveQuery = new ADWebServicesProcessCustomerDocumentsRemoveQuery();
        }
        #endregion

        #region - M E T H O D S
        /// <summary>
        /// Get all invoices document to remove
        /// </summary>
        /// <returns>List of ADWebServicesProcessCustomerDocumentEntity object with the data about invoices documents</returns>
        public IEnumerable<ADWebServicesProcessCustomerDocumentEntity> GetInvoiceToRemove()
        {
            return CustomerDocumentsRemoveQuery.GetInvoiceToRemove();
        }

        /// <summary>
        /// Get all delivery notes document to remove
        /// </summary>
        /// <returns>List of ADWebServicesProcessCustomerDocumentEntity object with the data about delivery note documents</returns>
        public IEnumerable<ADWebServicesProcessCustomerDocumentEntity> GetDeliveryNoteToRemove()
        {
            return CustomerDocumentsRemoveQuery.GetDeliveryNoteToRemove();
        }

        /// <summary>
        /// Get customer outstanding payment to set like removed
        /// </summary>
        /// <returns>Collection of ADWebServicesProcessCustomerDocumentEffectEtntity objects with data about effects documents</returns>
        public IEnumerable<ADWebServicesProcessCustomerDocumentEffectEtntity> GetEffectsToSetLikeDeleted()
        {
            return CustomerDocumentsRemoveQuery.GetEffectsToSetLikeDeleted();
        }

        /// <summary>
        /// Get collection string with customer code for removing outstanding payment file on FTP server
        /// </summary>
        /// <returns>Collection string with code customer</returns>
        public IEnumerable<string> GetCustomerToRemoveOutstandingPaymentsFile()
        {
            return CustomerDocumentsRemoveQuery.GetCustomerToRemoveOutstandingPaymentsFile();
        }

        /// <summary>
        /// Absolute path where is the customer outstanding payment
        /// </summary>
        /// <returns></returns>
        public string GetAbsolutePathCustomerOutstandingPaymentFolderOnFTPServer()
        {
            return CustomerDocumentsRemoveQuery.GetAbsolutePathCustomerOutstandingPaymentOnFTPServer();
        }

        /// <summary>
        /// Set document effect like removed
        /// </summary>
        /// <param name="EffectsCollection">Collection object that represent customer outstanding payments documents</param>
        public void SetEffectsLikeDeleted(IEnumerable<ADWebServicesProcessCustomerDocumentEffectEtntity> EffectsCollection)
        {
            foreach (var Effect in EffectsCollection)
            {
                CustomerDocumentsRemoveQuery.SetEffectsLikeDeleted(Effect);
            }
        }


        /// <summary>
        /// Save the log indicating invoice has been removed
        /// </summary>
        /// <param name="InvoiceCustomerEntity">Invoice information</param>
        public void AddLogDocumentRemoved(ADWebServicesProcessCustomerDocumentEntity InvoiceCustomerEntity)
        {
            CustomerDocumentsRemoveQuery.AddLogDocumentRemoved(InvoiceCustomerEntity);
        }

        /// <summary>
        /// Absolute path where is the software to create HTML table of customer document
        /// </summary>
        /// <returns>C:\Program Files\ccs\g733\ejecutables\CustomerDocumentExcelAndPdf.exe</returns>
        public string GetAbsolutePathOfSoftwareToCreateCustomerTableHTMLDocument()
        {
            return CustomerDocumentsRemoveQuery.GetAbsolutePathOfSoftwareToCreateCustomerTableHTMLDocument();
        }

        /// <summary>
        /// Get name folder to save customer document created
        /// </summary>
        /// <returns></returns>
        public string GetNameFolderToSaveCustomerDocumentCreated()
        {
            return CustomerDocumentsRemoveQuery.GetNameFolderToSaveCustomerDocumentCreated();
        }

        /// <summary>
        /// Get absolute path of invoices folder on FTP server
        /// </summary>
        /// <returns>Path of invoices folder on FTP server</returns>
        public string GetPathFolderInvoicesOnFTPServer()
        {
            return CustomerDocumentsRemoveQuery.GetPathFolderInvoicesOnFTPServer();
        }

        /// <summary>
        /// Get absolute path of delivery notes folder on FTP server
        /// </summary>
        /// <returns>Path of delivery notes folder on FTP server</returns>
        public string GetPathFolderDeliveryNoteOnFTPServer()
        {
            return CustomerDocumentsRemoveQuery.GetPathFolderDeliveryNoteOnFTPServer();
        }

        /// <summary>
        /// Get data connection FTP
        /// </summary>
        /// <returns>return data structure with required data to connect to FTP server</returns>
        public ADTool.Data.DataConnection GetFTPConnection()
        {
            return CustomerDocumentsRemoveQuery.GetFTPConnection();
        }

        /// <summary>
        /// Get structure name of invoices
        /// </summary>
        /// <returns>Structure name of invoices</returns>
        public string GetStructureNameOfInvoice()
        {
            return CustomerDocumentsRemoveQuery.GetStructureNameOfInvoice();
        }

        /// <summary>
        /// Get structure name of delivery notes
        /// </summary>
        /// <returns>Structure name of delivery notes</returns>
        public string GetStructureNameOfDeliveryNote()
        {
            return CustomerDocumentsRemoveQuery.GetStructureNameOfDeliveryNote();
        }

        /// <summary>
        /// Get Absolute path where is the documents folder customers
        /// </summary>
        /// <returns></returns>
        public string GetAbsolutePathToSaveCustomerDocumentCreated()
        {
            return new ADWebServicesCustomerDocumentsQuery().GetAbsolutePathToSaveCustomerDocumentCreated();
        }
        #region DISPOSE METHODS
        /// <summary>
        /// ADWebServicesProcessCustomerDocumentsRemoveRepository Destructor
        /// </summary>
        ~ADWebServicesProcessCustomerDocumentsRemoveRepository()
        {
            Dispose(false);
        }

        /// <summary>
        /// DatabaseController Destructor
        /// </summary>
        /// <summary>
        /// Dispose the DDBB.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            /* Take yourself off the Finalization queue to prevent finalization code
             * for this object from executing a second time.
             */
            GC.SuppressFinalize(this);
        }

        /* Dispose(bool disposing) executes in two distinct scenarios.
         * If disposing equals true, the method has been called directly or indirectly
         * by a user's code. Managed and unmanaged resources can be disposed.
         * If disposing equals false, the method has been called by the runtime from
         * inside the finalizer and you should not reference other objects. Only
         * unmanaged resources can be disposed.
         */
        /// <summary>
        /// Virtual dispose the DDBB.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    CustomerDocumentsRemoveQuery.Dispose();
                    CustomerDocumentsRemoveQuery = null;
                }
                /* Release unmanaged resources. If disposing is false, only the following code
                 * is executed. Note that this is not thread safe. Another thread could start
                 * disposing the object after the managed resources are disposed, but before
                 * the disposed flag is set to true. If thread safety is necessary, it must be
                 * implemented by the client.
                 */
            }
            disposed = true;
        }
        #endregion

        #endregion
    }
}
