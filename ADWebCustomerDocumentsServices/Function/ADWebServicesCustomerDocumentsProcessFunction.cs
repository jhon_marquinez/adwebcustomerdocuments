﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADWebCustomerDocumentsServices.Function
{
    internal class ADWebServicesCustomerDocumentsProcessFunction
    {
        #region - C O N S T R U C T O R S
        /// <summary>
        /// Constructor
        /// </summary>
        public ADWebServicesCustomerDocumentsProcessFunction()
        {
        }
        #endregion

        #region - M E T H O D S
        /// <summary>
        /// Run some software installed
        /// </summary>
        /// <param name="fileName">Absolute path where is the execute software</param>
        /// <param name="arguments">Parameters to execute the software</param>
        /// <param name="timeOut">Time out to close or kill process software</param>
        public static void RunProcessInBackground(string fileName, string arguments, int timeOut)
        {
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            startInfo.FileName = fileName;
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.Arguments = arguments;
            using (System.Diagnostics.Process Process = System.Diagnostics.Process.Start(startInfo))
            {
                Process.WaitForExit(timeOut);

                if (!Process.HasExited)
                    Process.Kill();
            }
        }
        #endregion
    }
}
