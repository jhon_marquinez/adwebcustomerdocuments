﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADWebWindowsServices.Manager;
using ADWebWindowsServices.Entity;
using ADWebCustomerDocumentsServices.Repository;
using ADWebCustomerDocumentsServices.Resouce;
using ADWebCustomerDocumentsServices.Process;
using ADTool.Log;
using ADTool.Serializer;

namespace ADWebCustomerDocumentsServices.Manager
{
    public class ADWebServicesCustomerDocumentsManager
    {
        #region - P R O P E R T I E S
        /// <summary>
        /// Instance to get info about the services like process and task
        /// </summary>
        private ADWebWindowsServicesManager ServicesConfiguration;
        #endregion

        #region - C O N S T R U C T O R
        /// <summary>
        /// Constructor
        /// </summary>
        public ADWebServicesCustomerDocumentsManager()
        {
            ServicesConfiguration = new ADWebWindowsServicesManager();
        }
        #endregion

        #region - M E T H O D S
        /// <summary>
        /// Get the services name
        /// </summary>
        /// <returns></returns>
        public string GetServicesName()
        {
            using (ADWebServicesCustomerDocumentsRepository CustomerDocumentsRepository = new ADWebServicesCustomerDocumentsRepository())
            {
                return CustomerDocumentsRepository.GetServicesName();
            }
        }

        /// <summary>
        /// Start the execution process and process's tasks
        /// </summary>
        public void Initialize()
        {
            ADWebWindowsServicesEntity Services = ServicesConfiguration.GetServices(GetServicesName());
            IEnumerable<ADWebWindowsServicesProcessEntity> ProcessRunning = ServicesConfiguration.GetProcessRunning(Services);
            if (ProcessRunning.Count() > 0)
                ServicesConfiguration.AddLogProcessRunning(ProcessRunning);

            IEnumerable<ADWebWindowsServicesProcessEntity> ProcessNotRunning = ServicesConfiguration.GetProcessNotRunning(Services);
            foreach (ADWebWindowsServicesProcessEntity Process in ProcessNotRunning)
            {
                if (!ServicesConfiguration.IsAllowExecutionThisProcessAfterCheckScheduleTime(Process)) break;

                ServicesConfiguration.StartProcess(Process);
                ServicesConfiguration.AddLog(Process, $"The process {Process.Name} has been started.");

                try
                {
                    ExecuteServicesProcess(Process);
                    ServicesConfiguration.AddLog(Process, $"The process {Process.Name} has been execute correctly.");
                }
                catch (Exception ex)
                {
                    ex = ex.InnerException ?? ex;
                    ADWebWindowsServicesException ADException =
                        new ADWebWindowsServicesException(
                            ex.TargetSite.ToString(),
                            ex.Message,
                            ex.Source,
                            ex.TargetSite.DeclaringType.ToString(),
                            ex.TargetSite.MemberType.ToString(),
                            ex.StackTrace
                        );
                    try { ServicesConfiguration.AddLog(Process, $"Error while the process {Process.Name} was executing.", LogType.Error, JsonSerializer.Serialize(ADException)); }
                    catch (Exception excep)
                    {
                        //TODO: Añadir log en fichero
                    }
                }

                ServicesConfiguration.StopProcess(Process);
                ServicesConfiguration.AddLog(Process, $"The process {Process.Name} has been stopped.");
            }

            RemoveAllDocumentCreatedInLocal();
            StopAllProcessRunning();
        }

        /// <summary>
        /// Remove all customer documents created in local for free space on server
        /// </summary>
        private void RemoveAllDocumentCreatedInLocal()
        {
            new ADWebServicesProcessCustomerDocumentsRemove().RemoveAllCustomerDocumentsInLocal();
        }

        /// <summary>
        /// Execute the process
        /// </summary>
        /// <param name="Process"></param>
        /// <exception cref="Exception"></exception>
        internal void ExecuteServicesProcess(ADWebWindowsServicesProcessEntity Process)
        {
            IEnumerable<ADWebWindowsServicesProcessTaskEntity> ActiveTask = ServicesConfiguration.GetTaskActive(Process);

            foreach (ADWebWindowsServicesProcessTaskEntity Task in ActiveTask)
            {
                try
                {
                    ServicesConfiguration.StartTask(Task);
                    ServicesConfiguration.AddLog(Task, $"The task {Task.Name} has been started.");

                    if (Process.Name == ConstantValue.ADWebWindowsServicesProcessCustomerDocumentsRemove)
                    {
                        new ADWebServicesProcessCustomerDocumentsRemove().RunTask(Task);
                        ServicesConfiguration.AddLog(Task, $"The task {Task.Name} has been execute correctly.");
                    }
                    else if (Process.Name == ConstantValue.ADWebWindowsServicesProcessCustomerDocumentsCreate)
                    {
                        new ADWebServicesProcessCustomerDocumentsCreate().RunTask(Task);
                        ServicesConfiguration.AddLog(Task, $"The task {Task.Name} has been execute correctly.");
                    }
                    else if (Process.Name == ConstantValue.ADWebWindowsServicesProcessCustomerDocumentsUpload)
                    {
                        new ADWebServicesProcessCustomerDocumentsUpload().RunTask(Task);
                        ServicesConfiguration.AddLog(Task, $"The task {Task.Name} has been execute correctly.");
                    }
                }
                catch (Exception ex)
                {
                    ex = ex.InnerException ?? ex;
                    ADWebWindowsServicesException ADException =
                        new ADWebWindowsServicesException(
                            ex.TargetSite.ToString(),
                            ex.Message,
                            ex.Source,
                            ex.TargetSite.DeclaringType.ToString(),
                            ex.TargetSite.MemberType.ToString(),
                            ex.StackTrace
                        );
                    try {
                        ServicesConfiguration.AddLog(Task, $"Error while the task {Process.Name} was executing.", LogType.Error, JsonSerializer.Serialize(ADException)); }
                    catch (Exception excep)
                    {
                        //TODO: Añadir log en fichero
                    }
                }
            }
        }

        /// <summary>
        /// Stop all process running
        /// </summary>
        public void StopAllProcessRunning()
        {
            ADWebWindowsServicesEntity Services = ServicesConfiguration.GetServices(GetServicesName());
            IEnumerable<ADWebWindowsServicesProcessEntity> ProcessRunning = ServicesConfiguration.GetProcessRunning(Services);
            if (ProcessRunning.Count() > 0)
                foreach (ADWebWindowsServicesProcessEntity Process in ProcessRunning)
                {
                    ServicesConfiguration.StopProcess(Process);
                    ServicesConfiguration.AddLog(Process, $"The process {Process.Name} has been stopped.");
                }
        }

        /// <summary>
        /// Insert services configuration data
        /// </summary>
        /// <returns>ADWebServicesCustomerDocumentsManager instance</returns>
        public ADWebServicesCustomerDocumentsManager InsertServicesConfigurationData()
        {
            using (ADWebServicesCustomerDocumentsRepository CustomerDocumentsRepository = new ADWebServicesCustomerDocumentsRepository())
            {
                CustomerDocumentsRepository.InsertServicesConfigurationData();
            }
            return this;
        }

        /// <summary>
        /// Insert process configuration data
        /// </summary>
        /// <returns>ADWebServicesCustomerDocumentsManager instance</returns>
        public ADWebServicesCustomerDocumentsManager InsertProcessConfigurationData()
        {
            using (ADWebServicesCustomerDocumentsRepository CustomerDocumentsRepository = new ADWebServicesCustomerDocumentsRepository())
            {
                CustomerDocumentsRepository.InsertProcessConfigurationData();
            }
            return this;
        }

        /// <summary>
        /// Insert task configuration data
        /// </summary>
        /// <returns>ADWebServicesCustomerDocumentsManager instance</returns>
        public ADWebServicesCustomerDocumentsManager InsertTaskConfigurationData()
        {
            using (ADWebServicesCustomerDocumentsRepository CustomerDocumentsRepository = new ADWebServicesCustomerDocumentsRepository())
            {
                CustomerDocumentsRepository.InsertTaskConfigurationData();
            }
            return this;
        }

        /// <summary>
        /// Delete all configuration services data
        /// </summary>
        /// <returns>ADWebServicesCustomerDocumentsManager instance</returns>
        public ADWebServicesCustomerDocumentsManager DeleteConfigurationData()
        {
            using (ADWebServicesCustomerDocumentsRepository CustomerDocumentsRepository = new ADWebServicesCustomerDocumentsRepository())
            {
                CustomerDocumentsRepository.DeleteServicesConfigurationData();
            }
            return this;
        }
        #endregion
    }
}
