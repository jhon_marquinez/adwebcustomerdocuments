﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADWebWindowsServices.Entity;
using ADWebCustomerDocumentsServices.Resouce;
using ADWebCustomerDocumentsServices.Repository;
using ADWebCustomerDocumentsServices.Entity;
using ADWebCustomerDocumentsServices.Enum;
using ADWebCustomerDocumentsServices.Function;
using ADTool.Ftp.FtpClient;
using System.IO;

namespace ADWebCustomerDocumentsServices.Process
{
    internal class ADWebServicesProcessCustomerDocumentsRemove
    {
        #region - P R O P E R T I E S
        /// <summary>
        /// Instance repository used to access to database
        /// </summary>
        private ADWebServicesProcessCustomerDocumentsRemoveRepository CustomerDocumentsRemoveRepository;
        /// <summary>
        /// Client FTP
        /// </summary>
        FtpClient _FTPClient = null;
        /// <summary>
        /// Time out to kill or exit process executed
        /// </summary>
        private const int timeOutRunProcess = 60000;
        #endregion

        #region - C O N S T R U C T O R
        /// <summary>
        /// Constructor
        /// </summary>
        public ADWebServicesProcessCustomerDocumentsRemove()
        {
            CustomerDocumentsRemoveRepository = new ADWebServicesProcessCustomerDocumentsRemoveRepository();
            _FTPClient = new FtpClient(CustomerDocumentsRemoveRepository.GetFTPConnection());
        }
        #endregion

        #region - M E T H O D S
        /// <summary>
        /// Execution task
        /// </summary>
        /// <param name="TaskEntity"></param>
        public void RunTask(ADWebWindowsServicesProcessTaskEntity TaskEntity)
        {
            if (TaskEntity.Name == ConstantValue.ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveDeliveryNote)
            {
                ServicesProcessCustomerDocumentsTaskRemoveDeliveryNote();
            }
            else if (TaskEntity.Name == ConstantValue.ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveInvoice)
            {
                ServicesProcessCustomerDocumentsTaskRemoveInvoice();
            }
            else if (TaskEntity.Name == ConstantValue.ADWebWindowsServicesProcessCustomerDocumentsTaskRemoveOutstandingPayment)
            {
                ServicesProcessCustomerDocumentsTaskRemoveOutstandingPayment();
            }
        }

        /// <summary>
        /// Remove delivery notes, create HTML table (list customer delivery notes) updated and upload it
        /// </summary>
        private void ServicesProcessCustomerDocumentsTaskRemoveDeliveryNote()
        {
            string pathDirectoryToSaveDocumentCreated = $"{CustomerDocumentsRemoveRepository.GetAbsolutePathToSaveCustomerDocumentCreated()}{CustomerDocumentsRemoveRepository.GetNameFolderToSaveCustomerDocumentCreated()}";
            Directory.CreateDirectory(pathDirectoryToSaveDocumentCreated);
            IEnumerable<ADWebServicesProcessCustomerDocumentEntity> DeliveryNoteCollection = CustomerDocumentsRemoveRepository.GetDeliveryNoteToRemove();
            foreach (var DeliveryNote in DeliveryNoteCollection)
            {
                if (RemoveDeliveryNote(DeliveryNote))
                {
                    CustomerDocumentsRemoveRepository.AddLogDocumentRemoved(DeliveryNote);
                    string CustomerDocumentExcelAndPdfPath = CustomerDocumentsRemoveRepository.GetAbsolutePathOfSoftwareToCreateCustomerTableHTMLDocument();
                    // Execute software to create HTML table for listing customer delivery note
                    ADWebServicesCustomerDocumentsProcessFunction.RunProcessInBackground(
                        CustomerDocumentExcelAndPdfPath,
                        String.Format(ConstantValue.PametersToCreateTableHtml, pathDirectoryToSaveDocumentCreated, DeliveryNote.Customer, (int)TypeDocument.DeliveryNote),
                        timeOutRunProcess
                        );

                    // Upload customer delivery notes HTML table
                    _FTPClient.Connecte();
                    _FTPClient.UploadFile(
                        $"{pathDirectoryToSaveDocumentCreated}{DeliveryNote.Customer}{ConstantValue.DeliveryNoteNameFolderLocal}{ConstantValue.NameFileHtmlDeliveryNoteTable}",
                        $"{String.Format(CustomerDocumentsRemoveRepository.GetPathFolderDeliveryNoteOnFTPServer(), DeliveryNote.Customer)}{ConstantValue.NameFileHtmlDeliveryNoteTable}"
                        );
                    _FTPClient.Disconnect();
                }
            }
            Directory.Delete(pathDirectoryToSaveDocumentCreated, true);
        }

        /// <summary>
        /// Remove invoices, create HTML table (list customer invoices) updated and upload it
        /// </summary>
        private void ServicesProcessCustomerDocumentsTaskRemoveInvoice()
        {
            string pathDirectoryToSaveDocumentCreated = $"{CustomerDocumentsRemoveRepository.GetAbsolutePathToSaveCustomerDocumentCreated()}{CustomerDocumentsRemoveRepository.GetNameFolderToSaveCustomerDocumentCreated()}";
            Directory.CreateDirectory(pathDirectoryToSaveDocumentCreated);
            IEnumerable<ADWebServicesProcessCustomerDocumentEntity> InvoiceCollection = CustomerDocumentsRemoveRepository.GetInvoiceToRemove();
            foreach (var Invoice in InvoiceCollection)
            {
                if (RemoveInvoice(Invoice))
                {
                    CustomerDocumentsRemoveRepository.AddLogDocumentRemoved(Invoice);
                    string CustomerDocumentExcelAndPdfPath = CustomerDocumentsRemoveRepository.GetAbsolutePathOfSoftwareToCreateCustomerTableHTMLDocument();
                    // Execute software to create HTML table for listing customer invoices
                    ADWebServicesCustomerDocumentsProcessFunction.RunProcessInBackground(
                        CustomerDocumentExcelAndPdfPath,
                        String.Format(ConstantValue.PametersToCreateTableHtml, pathDirectoryToSaveDocumentCreated, Invoice.Customer, (int)TypeDocument.Invoice),
                        timeOutRunProcess
                        );
                    // Upload customer invoices HTML table
                    _FTPClient.Connecte();
                    _FTPClient.UploadFile(
                        $"{pathDirectoryToSaveDocumentCreated}{Invoice.Customer}{ConstantValue.InvoicesNameFolderLocal}{ConstantValue.NameFileHtmlInvoicesTable}", 
                        $"{String.Format(CustomerDocumentsRemoveRepository.GetPathFolderInvoicesOnFTPServer(), Invoice.Customer)}{ConstantValue.NameFileHtmlInvoicesTable}"
                        );
                    _FTPClient.Disconnect();
                }
            }
            Directory.Delete(pathDirectoryToSaveDocumentCreated, true);
        }

        /// <summary>
        /// Remove invoice
        /// </summary>
        /// <param name="InvoiceCustomerEntity">Invoice information</param>
        private bool RemoveInvoice(ADWebServicesProcessCustomerDocumentEntity InvoiceCustomerEntity)
        {
            string pathFolderInvoiceOnFTPServer = String.Format(CustomerDocumentsRemoveRepository.GetPathFolderInvoicesOnFTPServer(), InvoiceCustomerEntity.Customer),
                nameInvoiceFile =
                    String.Format(
                        CustomerDocumentsRemoveRepository.GetStructureNameOfInvoice(),
                        InvoiceCustomerEntity.Customer,
                        InvoiceCustomerEntity.IdDocument,
                        InvoiceCustomerEntity.Date.ToString("ddMMyyyy"),
                        InvoiceCustomerEntity.TypeFile == ConstantValue.PDFName ? ConstantValue.PDFExtention : ConstantValue.ExcelExtention
                        );
            _FTPClient.Connecte();
            InvoiceCustomerEntity.IsRemoved = _FTPClient.RemoveFileOnFTPServer($"{pathFolderInvoiceOnFTPServer}{nameInvoiceFile}");
            _FTPClient.Disconnect();
            return InvoiceCustomerEntity.IsRemoved;
        }

        /// <summary>
        /// Remove delivery note
        /// </summary>
        /// <param name="DelivetyNoteCustomerEntity">Delivery note information</param>
        private bool RemoveDeliveryNote(ADWebServicesProcessCustomerDocumentEntity DelivetyNoteCustomerEntity)
        {
            //TODO: check this code
            string pathFileDeliveryNoteOnFTPServer = String.Format(CustomerDocumentsRemoveRepository.GetPathFolderDeliveryNoteOnFTPServer(), DelivetyNoteCustomerEntity.Customer),
                nameDeliveryNoteFile =
                    String.Format(
                        CustomerDocumentsRemoveRepository.GetStructureNameOfDeliveryNote(),
                        DelivetyNoteCustomerEntity.Customer,
                        DelivetyNoteCustomerEntity.IdDocument,
                        DelivetyNoteCustomerEntity.Date.ToString("ddMMyyyy"),
                        DelivetyNoteCustomerEntity.TypeFile == ConstantValue.PDFName ? ConstantValue.PDFExtention : ConstantValue.ExcelExtention
                        );
            _FTPClient.Connecte();
            DelivetyNoteCustomerEntity.IsRemoved = _FTPClient.RemoveFileOnFTPServer($"{pathFileDeliveryNoteOnFTPServer}{nameDeliveryNoteFile}");
            _FTPClient.Disconnect();
            return DelivetyNoteCustomerEntity.IsRemoved;
        }

        /// <summary>
        /// Remove outstanding payment customer
        /// </summary>
        private void ServicesProcessCustomerDocumentsTaskRemoveOutstandingPayment()
        {
            ServicesProcessCustomerDocumentsTaskRemoveOutstandingPayment_SetEffectsLikeDeleted();
            IEnumerable<string> CustomerToRemoveOutstandingPaymentsFileCollection = CustomerDocumentsRemoveRepository.GetCustomerToRemoveOutstandingPaymentsFile();

            _FTPClient.Connecte();
            foreach (string CustomerId in CustomerToRemoveOutstandingPaymentsFileCollection)
            {
                string pathFileOutstandingPayment = String.Format(CustomerDocumentsRemoveRepository.GetAbsolutePathCustomerOutstandingPaymentFolderOnFTPServer(), CustomerId);

                IEnumerable<string> itemsFile = null;
                try {
                    // if does not exists customer directory on FTP continue with next customer
                    itemsFile = _FTPClient.GetListFullName(pathFileOutstandingPayment);
                } catch (Exception ex) { continue; }

                foreach (string fileName in itemsFile)
                {
                    _FTPClient.RemoveFileOnFTPServer(fileName);
                }
            }
            _FTPClient.Disconnect();
        }

        /// <summary>
        /// Set effects like to remove
        /// </summary>
        private void ServicesProcessCustomerDocumentsTaskRemoveOutstandingPayment_SetEffectsLikeDeleted()
        {
            IEnumerable<ADWebServicesProcessCustomerDocumentEffectEtntity> EffectsCollection = CustomerDocumentsRemoveRepository.GetEffectsToSetLikeDeleted();
            CustomerDocumentsRemoveRepository.SetEffectsLikeDeleted(EffectsCollection);
        }

        /// <summary>
        /// Remove all customer documents in local
        /// </summary>
        public void RemoveAllCustomerDocumentsInLocal()
        {
            string pathDirectoryToSaveDocumentCreated = $"{CustomerDocumentsRemoveRepository.GetAbsolutePathToSaveCustomerDocumentCreated()}{CustomerDocumentsRemoveRepository.GetNameFolderToSaveCustomerDocumentCreated()}";
            Directory.Delete(pathDirectoryToSaveDocumentCreated, true);
        }
        #endregion
    }
}
