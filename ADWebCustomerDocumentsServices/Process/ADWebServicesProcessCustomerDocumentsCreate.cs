﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADWebWindowsServices.Entity;
using ADWebCustomerDocumentsServices.Resouce;
using ADWebCustomerDocumentsServices.Repository;
using ADWebCustomerDocumentsServices.Enum;
using ADWebCustomerDocumentsServices.Function;
using System.IO;

namespace ADWebCustomerDocumentsServices.Process
{
    internal class ADWebServicesProcessCustomerDocumentsCreate
    {
        #region - P R O P E R T I E S
        /// <summary>
        /// Instance to access to data base
        /// </summary>
        private ADWebServicesProcessCustomerDocumentsCreateRepository CustomerDocumentsCreateRepository;
        /// <summary>
        /// Time out to kill or exit process executed
        /// </summary>
        private const int timeOutRunProcess = 21600000;
        #endregion

        #region - C O N S T R U C T O R S
        /// <summary>
        /// Constructor
        /// </summary>
        public ADWebServicesProcessCustomerDocumentsCreate()
        {
            CustomerDocumentsCreateRepository = new ADWebServicesProcessCustomerDocumentsCreateRepository();
        }
        #endregion

        #region - M E T H O D S
        /// <summary>
        /// Execute task
        /// </summary>
        /// <param name="TaskEntity"></param>
        public void RunTask(ADWebWindowsServicesProcessTaskEntity TaskEntity)
        {
            if (TaskEntity.Name == ConstantValue.ADWebWindowsServicesProcessCustomerDocumentsTaskCreateDeliveryNote)
            {
                ServicesProcessCustomerDocumentsTaskCreateDeliveryNote();
            }
            else if (TaskEntity.Name == ConstantValue.ADWebWindowsServicesProcessCustomerDocumentsTaskCreateInvoice)
            {
                ServicesProcessCustomerDocumentsTaskCreateInvoice();
            }
            else if (TaskEntity.Name == ConstantValue.ADWebWindowsServicesProcessCustomerDocumentsTaskCreateOutstandingPayment)
            {
                ServicesProcessCustomerDocumentsTaskCreateOutstandingPayment();
            }
            else if (TaskEntity.Name == ConstantValue.ADWebWindowsServicesProcessCustomerDocumentsTaskCreateSalesManOrder)
            {
                ServicesProcessCustomerDocumentsTaskCreateSalesManOrder();
            }
        }

        /// <summary>
        /// Create delivery note
        /// </summary>
        private void ServicesProcessCustomerDocumentsTaskCreateDeliveryNote()
        {
            string pathDirectoryToSaveDocumentCreated = $"{CustomerDocumentsCreateRepository.GetAbsolutePathToSaveCustomerDocumentCreated()}{CustomerDocumentsCreateRepository.GetNameFolderToSaveCustomerDocumentCreated()}";
            Directory.CreateDirectory(pathDirectoryToSaveDocumentCreated);
            string CustomerDocumentExcelAndPdfPath = CustomerDocumentsCreateRepository.GetAbsolutePathOfSoftwareToCreateCustomerDocuments();

            ADWebServicesCustomerDocumentsProcessFunction.RunProcessInBackground(
                CustomerDocumentExcelAndPdfPath,
                String.Format(ConstantValue.PametersToCreateDeliveryNoteDocuments, pathDirectoryToSaveDocumentCreated, (int)TypeDocument.DeliveryNote),
                timeOutRunProcess
            );
        }

        /// <summary>
        /// Create invoices
        /// </summary>
        private void ServicesProcessCustomerDocumentsTaskCreateInvoice()
        {
            string pathDirectoryToSaveDocumentCreated = $"{CustomerDocumentsCreateRepository.GetAbsolutePathToSaveCustomerDocumentCreated()}{CustomerDocumentsCreateRepository.GetNameFolderToSaveCustomerDocumentCreated()}";
            Directory.CreateDirectory(pathDirectoryToSaveDocumentCreated);
            string CustomerDocumentExcelAndPdfPath = CustomerDocumentsCreateRepository.GetAbsolutePathOfSoftwareToCreateCustomerDocuments();

            ADWebServicesCustomerDocumentsProcessFunction.RunProcessInBackground(
                CustomerDocumentExcelAndPdfPath,
                String.Format(
                    ConstantValue.PametersToCreateInvoiceDocuments,
                    pathDirectoryToSaveDocumentCreated,
                    (int)TypeDocument.Invoice,
                    DateTime.Now.AddYears(-1).Year,
                    DateTime.Now.Year
                    ),
                timeOutRunProcess
            );
        }

        /// <summary>
        /// Create outstanding payments customer
        /// </summary>
        private void ServicesProcessCustomerDocumentsTaskCreateOutstandingPayment()
        {
            string pathDirectoryToSaveDocumentCreated = $"{CustomerDocumentsCreateRepository.GetAbsolutePathToSaveCustomerDocumentCreated()}{CustomerDocumentsCreateRepository.GetNameFolderToSaveCustomerDocumentCreated()}";
            Directory.CreateDirectory(pathDirectoryToSaveDocumentCreated);
            string CustomerDocumentExcelAndPdfPath = CustomerDocumentsCreateRepository.GetAbsolutePathOfSoftwareToCreateCustomerDocuments();

            ADWebServicesCustomerDocumentsProcessFunction.RunProcessInBackground(
                CustomerDocumentExcelAndPdfPath,
                String.Format(ConstantValue.PametersToCreateOutstandingPaymentDocuments, pathDirectoryToSaveDocumentCreated, (int)TypeDocument.OutstandingPayment),
                timeOutRunProcess
            );
        }

        /// <summary>
        /// Create sales man orders
        /// </summary>
        private void ServicesProcessCustomerDocumentsTaskCreateSalesManOrder()
        {
            string pathDirectoryToSaveDocumentCreated = $"{CustomerDocumentsCreateRepository.GetAbsolutePathToSaveCustomerDocumentCreated()}{CustomerDocumentsCreateRepository.GetNameFolderToSaveCustomerDocumentCreated()}";
            Directory.CreateDirectory(pathDirectoryToSaveDocumentCreated);
            string CustomerDocumentExcelAndPdfPath = CustomerDocumentsCreateRepository.GetAbsolutePathOfSoftwareToCreateCustomerDocuments();

            ADWebServicesCustomerDocumentsProcessFunction.RunProcessInBackground(
                CustomerDocumentExcelAndPdfPath,
                String.Format(ConstantValue.PametersToCreateSalesmanOrdersDocuments, pathDirectoryToSaveDocumentCreated, (int)TypeDocument.SalesmanOrder),
                timeOutRunProcess
            );
        }
        #endregion
    }
}
