﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADWebWindowsServices.Entity;
using ADWebCustomerDocumentsServices.Resouce;
using ADWebCustomerDocumentsServices.Repository;
using ADTool.Ftp.FtpClient;
using System.IO;
using System.Text.RegularExpressions;
using ADWebWindowsServices.Manager;

namespace ADWebCustomerDocumentsServices.Process
{
    internal class ADWebServicesProcessCustomerDocumentsUpload
    {
        #region - P R O P E R T I E S 
        /// <summary>
        /// Instance to access to data base
        /// </summary>
        private ADWebServicesProcessCustomerDocumentsUploadRepository CustomerDocumentsUploadRepository;
        /// <summary>
        /// Client FTP
        /// </summary>
        FtpClient _FTPClient = null;
        #endregion

        #region - C O N S T R U C T O R S
        /// <summary>
        /// Constructor
        /// </summary>
        public ADWebServicesProcessCustomerDocumentsUpload()
        {
            CustomerDocumentsUploadRepository = new ADWebServicesProcessCustomerDocumentsUploadRepository();
            _FTPClient = new FtpClient(CustomerDocumentsUploadRepository.GetFTPConnection());
        }
        #endregion

        #region - M E T H O D S
        /// <summary>
        /// Execute task
        /// </summary>
        /// <param name="TaskEntity"></param>
        public void RunTask(ADWebWindowsServicesProcessTaskEntity TaskEntity)
        {
            if (TaskEntity.Name == ConstantValue.ADWebWindowsServicesProcessCustomerDocumentsTaskUploadDeliveryNote)
            {
                ServicesProcessCustomerDocumentsTaskUploadDeliveryNote();
            }
            else if (TaskEntity.Name == ConstantValue.ADWebWindowsServicesProcessCustomerDocumentsTaskUploadInvoice)
            {
                ServicesProcessCustomerDocumentsTaskUploadInvoice();
            }
            else if (TaskEntity.Name == ConstantValue.ADWebWindowsServicesProcessCustomerDocumentsTaskUploadOutstandingPayment)
            {
                ServicesProcessCustomerDocumentsTaskUploadOutstandingPayment();
            }
            else if (TaskEntity.Name == ConstantValue.ADWebWindowsServicesProcessCustomerDocumentsTaskUploadSalesManOrder)
            {
                ServicesProcessCustomerDocumentsTaskUploadSalesManOrder();
            }
        }

        /// <summary>
        /// Upload delivery notes
        /// </summary>
        private void ServicesProcessCustomerDocumentsTaskUploadDeliveryNote()
        {
            UploadDocument(ConstantValue.DeliveryNoteNameFolderLocal, CustomerDocumentsUploadRepository.GetPathDeliveryNoteFolderOnFTPServer(), false);
        }

        /// <summary>
        /// Upload invoices
        /// </summary>
        private void ServicesProcessCustomerDocumentsTaskUploadInvoice()
        {
            UploadDocument(ConstantValue.InvoicesNameFolderLocal, CustomerDocumentsUploadRepository.GetPathInvoicesFolderOnFTPServer(), false);
        }

        /// <summary>
        /// Upload outstanding payments
        /// </summary>
        private void ServicesProcessCustomerDocumentsTaskUploadOutstandingPayment()
        {
            UploadDocument(ConstantValue.OutstandingPaymentsNameFolderLocal, CustomerDocumentsUploadRepository.GetPathCustomerOutstandingPaymentFolderOnFTPServer(), false);
        }

        /// <summary>
        /// Upload sales man orders
        /// </summary>
        private void ServicesProcessCustomerDocumentsTaskUploadSalesManOrder()
        {
            UploadDocument(ConstantValue.SalesManOrdersNameFolderLocal, CustomerDocumentsUploadRepository.GetPathSalesmanOrders(), true);
        }

        /// <summary>
        /// Upload customer documents
        /// </summary>
        /// <param name="nameDocumentFolderLocal">name folder local where is saved the document to upload</param>
        /// <param name="documentFolderRemote">name folder remote where will save the document</param>
        private void UploadDocument(string nameDocumentFolderLocal, string documentFolderRemote, bool isSalesMan)
        {
            string pathDirectoryToSaveDocumentCreated = $"{CustomerDocumentsUploadRepository.GetAbsolutePathToSaveCustomerDocumentCreated()}{CustomerDocumentsUploadRepository.GetNameFolderToSaveCustomerDocumentCreated()}";

            string[] subDirectoriesArray = 
                isSalesMan 
                ? Directory.GetDirectories(pathDirectoryToSaveDocumentCreated)
                    .Where(directoryCodeCustomerPath => new Regex($@"^{ConstantValue.PrefixCodeSalesMan}\d*$").IsMatch(directoryCodeCustomerPath.Substring(directoryCodeCustomerPath.Length - 5)))
                    .ToArray() 
                : Directory.GetDirectories(pathDirectoryToSaveDocumentCreated);

            foreach (string subDirectoryCodeCustomerPath in subDirectoriesArray)
            {
                string customerId = subDirectoryCodeCustomerPath.Substring(subDirectoryCodeCustomerPath.Length - 5);

                string[] nameFilesArray = Directory.GetFiles($"{subDirectoryCodeCustomerPath}{nameDocumentFolderLocal}");
                if (nameFilesArray.Length > 0)
                {
                    if (!StructureFolderCustomerOnFTPServerIsOK(customerId))
                        CreateStructureFolderCustomerOnFTPServer(customerId);
                    _FTPClient.Connecte();
                    foreach (string filePath in nameFilesArray)
                    {
                        var nameFile = filePath.Split('\\').LastOrDefault();
                        try
                        {
                            // If occurs some exception while is uploading some file to FTP Server, continue with the next file
                            _FTPClient.UploadFile(
                                filePath,
                                $"{String.Format(documentFolderRemote, customerId)}{nameFile}"
                                );
                        }
                        catch (Exception ex){
                            //TODO: add log about file was not uploaded
                            continue;
                        }
                    }
                    _FTPClient.Disconnect();
                }
            }
        }

        /// <summary>
        /// Check if structure folder remote exists
        /// </summary>
        /// <param name="customerId">Code customer to check his structure directory</param>
        /// <returns>true if all directories are created</returns>
        private bool StructureFolderCustomerOnFTPServerIsOK(string customerId)
        {
            _FTPClient.Connecte();
            if (!_FTPClient.DirectoryExists($"{CustomerDocumentsUploadRepository.GetPathFolderRootCustomerDocumentsOnFTPServer()}{customerId}")) return false;
            if (!_FTPClient.DirectoryExists(String.Format(CustomerDocumentsUploadRepository.GetPathOrderFolderOnFTPServer(), customerId))) return false;
            if (!_FTPClient.DirectoryExists(String.Format(CustomerDocumentsUploadRepository.GetPathDeliveryNoteFolderOnFTPServer(), customerId))) return false;
            if (!_FTPClient.DirectoryExists(String.Format(CustomerDocumentsUploadRepository.GetPathInvoicesFolderOnFTPServer(), customerId))) return false;
            if (!_FTPClient.DirectoryExists(String.Format(CustomerDocumentsUploadRepository.GetPathCustomerOutstandingPaymentFolderOnFTPServer(), customerId))) return false;
            if (!_FTPClient.DirectoryExists(String.Format(CustomerDocumentsUploadRepository.GetPathCustomerConsumptionFolderOnFTPServer(), customerId))) return false;
            _FTPClient.Disconnect();
            return true;
        }

        /// <summary>
        /// Create structure folder remote for a client
        /// </summary>
        /// <param name="customerId">Code customer to create his structure directory</param>
        private void CreateStructureFolderCustomerOnFTPServer(string customerId)
        {
            _FTPClient.Connecte();
            _FTPClient.CreateDirectory(String.Format(CustomerDocumentsUploadRepository.GetPathOrderFolderOnFTPServer(), customerId));
            _FTPClient.CreateDirectory(String.Format(CustomerDocumentsUploadRepository.GetPathDeliveryNoteFolderOnFTPServer(), customerId));
            _FTPClient.CreateDirectory(String.Format(CustomerDocumentsUploadRepository.GetPathInvoicesFolderOnFTPServer(), customerId));
            _FTPClient.CreateDirectory(String.Format(CustomerDocumentsUploadRepository.GetPathCustomerOutstandingPaymentFolderOnFTPServer(), customerId));
            _FTPClient.CreateDirectory(String.Format(CustomerDocumentsUploadRepository.GetPathCustomerConsumptionFolderOnFTPServer(), customerId));
            _FTPClient.Disconnect();
        }
        #endregion
    }
}
